﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mid_Project_DB.Evaluations
{
    public partial class EditGroupEvaluation : Form
    {
        private int EVID;
        private int GroupID;
        private float Totalmarks;
        public EditGroupEvaluation(int EvaluationID , int GroupID ,float Marks , float totalMarks)
        {
            InitializeComponent();
            this.EVID= EvaluationID;
            this.GroupID= GroupID;
            txtUpdate.Text = Marks.ToString();
            Totalmarks = totalMarks;
        }

        private void EditGroupEvaluation_Load(object sender, EventArgs e)
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            this.CenterToScreen();
        }

        private void guna2Button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private bool checkID(string s)
        {
            for (int i = 0; i < s.Length; i++)
            {
                if (!Char.IsDigit(s[i]) && s[i] != '.')
                {
                    return false;
                }
            }
            return true;
        }

        private void guna2Button2_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtUpdate.Text))
            {
                if (checkID(txtUpdate.Text))
                {
                    if (float.Parse(txtUpdate.Text) <= Totalmarks)
                    {
                        var con = Configuration.getInstance().getConnection();
                        SqlCommand cmd1 = new SqlCommand("update [dbo].[GroupEvaluation] set ObtainedMarks = @Marks where GroupId = @ID AND EvaluationId = @EVID", con);
                        cmd1.Parameters.AddWithValue("@ID", GroupID);
                        cmd1.Parameters.AddWithValue("@EVID", EVID);
                        cmd1.Parameters.AddWithValue("@Marks", (txtUpdate.Text));
                        cmd1.Connection = con;
                        cmd1.ExecuteNonQuery();
                        MessageBox.Show("Marks Updated Successfully...");
                        this.Close();
                    }
                    else
                    {
                        S.SetError(txtUpdate, "Cannot get more than total marks!");

                    }

                }
                else
                {
                    S.SetError(txtUpdate, "Must enter only numbers!");

                }
            }
            else
            {
                S.SetError(txtUpdate , "TextBox cannot be empty!");
            }
        }
    }
}
