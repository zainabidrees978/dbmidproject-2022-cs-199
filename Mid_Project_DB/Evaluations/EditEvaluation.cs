﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mid_Project_DB.Evaluations
{
    public partial class EditEvaluation : Form
    {
        private int ID;
        private float totalW;
        public EditEvaluation(int ID , string name , float totalMarks , float totalweightage)
        {
            InitializeComponent();
            this.ID = ID;
            txtMarks.Text = totalMarks.ToString();
            txtName.Text = name;
            txtWeightage.Text = totalweightage.ToString();
            totalW = totalweightage;
        }

        private void EditEvaluation_Load(object sender, EventArgs e)
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            this.CenterToScreen();
        }

        private void guna2Button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private float getWeightage()
        {
            float weightage = 0;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select SUM(E.TotalWeightage) as Ev\r\nfrom Evaluation as E", con);
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            weightage = int.Parse(dataTable.Rows[0]["Ev"].ToString());
            weightage = weightage - totalW;
            return weightage;
        }
        private bool checkID(string s)
        {
            for (int i = 0; i < s.Length; i++)
            {
                if (!Char.IsDigit(s[i]))
                {
                    return false;
                }
            }
            return true;
        }

        private void guna2Button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtName.Text) || string.IsNullOrEmpty(txtMarks.Text) || string.IsNullOrEmpty(txtWeightage.Text))
            {
                MessageBox.Show("Please fill all queries...");
            }
            else
            {
                if (!checkID(txtMarks.Text))
                {
                    errorProvider1.SetError(txtMarks, "Please Enter Only Digits!");
                }
                else if (!checkID(txtWeightage.Text))
                {
                    errorProvider1.SetError(txtWeightage, "Please Enter Only Digits!");
                }
                else
                {
                    if (int.Parse(txtWeightage.Text) + getWeightage() > 100)
                    {
                        MessageBox.Show("Total weightage cannot increase more than 100...\nTotal Weightage Left: " + (100 - getWeightage()));
                    }
                    else
                    {
                        var con = Configuration.getInstance().getConnection();
                        SqlCommand cmd = new SqlCommand("update [dbo].[Evaluation] set Name = @Name , TotalMarks = @TotalMarks , TotalWeightage = @TotalWeightage where Id = @ID", con);
                        cmd.Parameters.AddWithValue("@Name", txtName.Text);
                        cmd.Parameters.AddWithValue("@TotalMarks", txtMarks.Text);
                        cmd.Parameters.AddWithValue("@TotalWeightage", txtWeightage.Text);
                        cmd.Parameters.AddWithValue("@ID", this.ID);
                        cmd.ExecuteNonQuery();
                        MessageBox.Show("Successfully Updated");
                        this.Close();
                    }

                }
            }
        }
    }
}
