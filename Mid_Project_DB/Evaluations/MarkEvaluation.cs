﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mid_Project_DB.Evaluations
{
    public partial class MarkEvaluation : Form
    {
        private int GroupID = -1;
        private int EvaluationId = -1;
        private BL.Evaluation evaluation = null;
        private float totalMarks;
        public MarkEvaluation()
        {
            InitializeComponent();
        }

        private void MarkEvaluation_Load(object sender, EventArgs e)
        {
            //ALL GROUPS DATA
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select DISTINCT Id , Created_On\r\nfrom [dbo].[Group]", con);
            cmd.Connection = con;
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            AllGroups.DataSource = dataTable;
            AllGroups.RowHeadersVisible = true;
            AllGroups.RowHeadersWidth = 30;

            //ALL EVALUATIONS DATA
            /*            SqlCommand cmd1 = new SqlCommand("select * from Evaluation", con);
                        cmd1.Connection = con;
                        SqlDataReader sqlData1 = cmd1.ExecuteReader();
                        DataTable dataTable1 = new DataTable();
                        dataTable1.Load(sqlData1);
                        AllEvaluations.DataSource = dataTable1;
                        AllEvaluations.RowHeadersVisible = true;*/
        }

        private void AllGroups_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            GroupID = int.Parse(AllGroups.Rows[e.RowIndex].Cells[0].Value.ToString());
            SqlCommand cmd1 = new SqlCommand("select * from [dbo].[Group] where Id = @ID", con);
            cmd1.Parameters.AddWithValue("ID", GroupID);
            cmd1.Connection = con;
            SqlDataReader sqlData1 = cmd1.ExecuteReader();
            DataTable dataTable1 = new DataTable();
            dataTable1.Load(sqlData1);
            SelectedGroup.DataSource = dataTable1;

            //ALL EVALUATIONS DATA
            SqlCommand cmd = new SqlCommand("select * \r\nfrom Evaluation as E\r\n\r\nEXCEPT\r\n\r\nselect E.Id , E.Name , E.TotalMarks , E.TotalWeightage\r\nfrom Evaluation as E\r\njoin GroupEvaluation as Ge\r\non Ge.EvaluationId = E.Id\r\njoin [dbo].[Group] as G\r\non G.Id = Ge.GroupId\r\nwhere G.Id = @ID", con);
            cmd.Connection = con;
            cmd.Parameters.AddWithValue("ID", GroupID);

            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            AllEvaluations.DataSource = dataTable;
            AllEvaluations.RowHeadersVisible =true;
            AllEvaluations.RowHeadersWidth = 30;
        }

        private void guna2Button3_Click(object sender, EventArgs e)
        {
            refreshAllGroups();
        }

        private void refreshAllGroups()
        {
            AllGroups.DataSource = null;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select DISTINCT Id , Created_On\r\nfrom [dbo].[Group]", con);
            cmd.Connection = con;
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            AllGroups.DataSource = dataTable;
        }

        private void refreshAllEvaluations()
        {
            AllEvaluations.DataSource = null;
            var con = Configuration.getInstance().getConnection();

            SqlCommand cmd = new SqlCommand("select * \r\nfrom Evaluation as E\r\n\r\nEXCEPT\r\n\r\nselect E.Id , E.Name , E.TotalMarks , E.TotalWeightage\r\nfrom Evaluation as E\r\njoin GroupEvaluation as Ge\r\non Ge.EvaluationId = E.Id\r\njoin [dbo].[Group] as G\r\non G.Id = Ge.GroupId\r\nwhere G.Id = @ID", con);
            cmd.Connection = con;
            cmd.Parameters.AddWithValue("ID", GroupID);

            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            AllEvaluations.DataSource = dataTable;
        }

        private void ProjectSearch_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtEvaluationSearch.Text))
            {
                AllEvaluations.DataSource = null;
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("select * from [dbo].[Evaluation] where Name = @ID");
                cmd.Parameters.AddWithValue("@ID", txtEvaluationSearch.Text);
                cmd.Connection = con;
                SqlDataReader sqlData = cmd.ExecuteReader();
                DataTable dataTable = new DataTable();
                dataTable.Load(sqlData);
                AllEvaluations.DataSource = dataTable;
            }
        }

        private void txtProjectSearch_KeyUp(object sender, KeyEventArgs e)
        {

        }

        private void guna2Button4_Click(object sender, EventArgs e)
        {
            refreshAllEvaluations();
        }

        private void StudentSearch_Click(object sender, EventArgs e)
        {
            BL.Group group = null;
            try
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("select G.Id as GroupID , G.Created_On ,S.Id as StudentID , S.RegistrationNo\r\nfrom [dbo].[Group] as G\r\njoin [dbo].GroupStudent as Gs\r\non Gs.GroupId = G.Id\r\njoin Lookup as L\r\non L.Id = Gs.Status\r\njoin Person as P\r\non P.Id = Gs.StudentId\r\njoin Student as S\r\non S.Id = P.Id\r\nwhere L.Value = 'Active'\r\ngroup by G.Id , G.Created_On , S.RegistrationNo , S.Id\r\n\r\nINTERSECT\r\n\r\nselect top 1 G.Id , G.Created_On ,S.Id , S.RegistrationNo\r\nfrom [dbo].[Group] as G\r\njoin [dbo].GroupStudent as Gs\r\non Gs.GroupId = G.Id\r\njoin Lookup as L\r\non L.Id = Gs.Status\r\njoin Person as P\r\non P.Id = Gs.StudentId\r\njoin Student as S\r\non S.Id = P.Id\r\nwhere L.Value = 'Active'\r\ngroup by G.Id , G.Created_On , S.RegistrationNo , S.Id", con);
                cmd.Connection = con;
                SqlDataReader sqlData = cmd.ExecuteReader();
                DataTable dataTable = new DataTable();
                dataTable.Load(sqlData);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    if (txtGroupSearch.Text == dataTable.Rows[i]["GroupID"].ToString())
                    {
                        group = new BL.Group(int.Parse(dataTable.Rows[i]["GroupID"].ToString()), dataTable.Rows[i]["Created_On"].ToString(), int.Parse(dataTable.Rows[i]["StudentID"].ToString()), dataTable.Rows[i]["RegistrationNo"].ToString());
                    }
                }
                AllGroups.DataSource = group;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void guna2Button1_Click(object sender, EventArgs e)
        {
            SelectedGroup.DataSource = null;
        }

        private void ProjectDelete_Click(object sender, EventArgs e)
        {
            SelectedEvaluation.DataSource = null;
        }
        private bool checkID(string s)
        {
            for (int i = 0; i < s.Length; i++)
            {
                if (!Char.IsDigit(s[i]) && s[i] != '.')
                {
                    return false;
                }
            }
            return true;
        }

        private void AddEvaluation_Click(object sender, EventArgs e)
        {
            if (SelectedEvaluation.DataSource == null)
            {
                MessageBox.Show("Please Select an Evaluation...");
            }
            else if (SelectedGroup.DataSource == null)
            {
                MessageBox.Show("Please Select a Group...");
            }
            else 
            {
                if (!string.IsNullOrEmpty(txtMarks.Text))
                {
                    if (checkID(txtMarks.Text))
                    {
                        if (float.Parse(txtMarks.Text) <= totalMarks)
                        {
                            try
                            {
                                var con = Configuration.getInstance().getConnection();
                                SqlCommand cmd = new SqlCommand("insert into [dbo].GroupEvaluation(GroupId , EvaluationId , ObtainedMarks , EvaluationDate) values(@GroupID , @EvaluationID , @ObtainedMarks , @Date)");
                                cmd.Parameters.AddWithValue("@GroupID", GroupID);
                                cmd.Parameters.AddWithValue("@EvaluationID", EvaluationId);
                                cmd.Parameters.AddWithValue("@ObtainedMarks", float.Parse(txtMarks.Text));
                                cmd.Parameters.AddWithValue("@Date", DateTime.Now.ToString("yyyy-MM-dd"));
                                cmd.Connection = con;
                                cmd.ExecuteNonQuery();
                                MessageBox.Show("Evaluation Has Been Marked...");
                                txtMarks.Text = "";
                                refreshAllEvaluations();
                                SelectedEvaluation.DataSource = null;
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message);
                            }
                        }
                        else
                        {
                            errorProvider1.SetError(txtMarks , "Cannot give more than total marks!");
                        }
                    }
                    else
                    {
                        errorProvider1.SetError(txtMarks, "Marks can only contain numbers!");
                    }
                }
                else
                {
                    errorProvider1.SetError(txtMarks, "Fill this query!");
                }
            }

        }

        private void AllGroups_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            GroupID = int.Parse(AllGroups.Rows[e.RowIndex].Cells[0].Value.ToString());
            SqlCommand cmd1 = new SqlCommand("select * from [dbo].[Group] where Id = @ID", con);
            cmd1.Parameters.AddWithValue("ID", GroupID);
            cmd1.Connection = con;
            SqlDataReader sqlData1 = cmd1.ExecuteReader();
            DataTable dataTable1 = new DataTable();
            dataTable1.Load(sqlData1);
            SelectedGroup.DataSource = dataTable1;

            //ALL EVALUATIONS DATA
            SqlCommand cmd = new SqlCommand("select * \r\nfrom Evaluation as E\r\n\r\nEXCEPT\r\n\r\nselect E.Id , E.Name , E.TotalMarks , E.TotalWeightage\r\nfrom Evaluation as E\r\njoin GroupEvaluation as Ge\r\non Ge.EvaluationId = E.Id\r\njoin [dbo].[Group] as G\r\non G.Id = Ge.GroupId\r\nwhere G.Id = @ID", con);
            cmd.Connection = con;
            cmd.Parameters.AddWithValue("ID", GroupID);

            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            AllEvaluations.DataSource = dataTable;
            AllEvaluations.RowHeadersVisible = true;
            AllEvaluations.RowHeadersWidth = 30;
        }

        private void AllEvaluations_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            EvaluationId = int.Parse(AllEvaluations.Rows[e.RowIndex].Cells[0].Value.ToString());
            totalMarks = float.Parse(AllEvaluations.Rows[e.RowIndex].Cells[2].Value.ToString());
            SqlCommand cmd1 = new SqlCommand("select * from [dbo].[Evaluation] where Id = @ID", con);
            cmd1.Parameters.AddWithValue("ID", EvaluationId);
            cmd1.Connection = con;
            SqlDataReader sqlData1 = cmd1.ExecuteReader();
            DataTable dataTable1 = new DataTable();
            dataTable1.Load(sqlData1);
            SelectedEvaluation.DataSource = dataTable1;
        }

        private void txtGroupSearch_KeyUp_1(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13)
            {
                BL.Group group = null;
                try
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("select G.Id as GroupID , G.Created_On ,S.Id as StudentID , S.RegistrationNo\r\nfrom [dbo].[Group] as G\r\njoin [dbo].GroupStudent as Gs\r\non Gs.GroupId = G.Id\r\njoin Lookup as L\r\non L.Id = Gs.Status\r\njoin Person as P\r\non P.Id = Gs.StudentId\r\njoin Student as S\r\non S.Id = P.Id\r\nwhere L.Value = 'Active'\r\ngroup by G.Id , G.Created_On , S.RegistrationNo , S.Id\r\n\r\nINTERSECT\r\n\r\nselect top 1 G.Id , G.Created_On ,S.Id , S.RegistrationNo\r\nfrom [dbo].[Group] as G\r\njoin [dbo].GroupStudent as Gs\r\non Gs.GroupId = G.Id\r\njoin Lookup as L\r\non L.Id = Gs.Status\r\njoin Person as P\r\non P.Id = Gs.StudentId\r\njoin Student as S\r\non S.Id = P.Id\r\nwhere L.Value = 'Active'\r\ngroup by G.Id , G.Created_On , S.RegistrationNo , S.Id", con);
                    cmd.Connection = con;
                    SqlDataReader sqlData = cmd.ExecuteReader();
                    DataTable dataTable = new DataTable();
                    dataTable.Load(sqlData);
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        if (txtGroupSearch.Text == dataTable.Rows[i]["GroupID"].ToString())
                        {
                            group = new BL.Group(int.Parse(dataTable.Rows[i]["GroupID"].ToString()), dataTable.Rows[i]["Created_On"].ToString(), int.Parse(dataTable.Rows[i]["StudentID"].ToString()), dataTable.Rows[i]["RegistrationNo"].ToString());
                        }
                    }
                    SelectedGroup.DataSource = group;
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void txtEvaluationSearch_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13)
            {
                try
                {
                    AllEvaluations.DataSource = null;
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("select * from [dbo].[Evaluation] where Name = @ID");
                    cmd.Parameters.AddWithValue("@ID", txtEvaluationSearch.Text);
                    cmd.Connection = con;
                    SqlDataReader sqlData = cmd.ExecuteReader();
                    DataTable dataTable = new DataTable();
                    dataTable.Load(sqlData);
                    AllEvaluations.DataSource = dataTable;
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void AllEvaluations_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            EvaluationId = int.Parse(AllEvaluations.Rows[e.RowIndex].Cells[0].Value.ToString());
            totalMarks = float.Parse(AllEvaluations.Rows[e.RowIndex].Cells[2].Value.ToString());
            SqlCommand cmd1 = new SqlCommand("select * from [dbo].[Evaluation] where Id = @ID", con);
            cmd1.Parameters.AddWithValue("ID", EvaluationId);
            cmd1.Connection = con;
            SqlDataReader sqlData1 = cmd1.ExecuteReader();
            DataTable dataTable1 = new DataTable();
            dataTable1.Load(sqlData1);
            SelectedEvaluation.DataSource = dataTable1;
        }
    }
}
