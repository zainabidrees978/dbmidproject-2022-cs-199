﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mid_Project_DB.Evaluations
{
    public partial class ViewGroupEvaluations : Form
    {
        public ViewGroupEvaluations()
        {
            InitializeComponent();
        }

        private int GroupID = -1;
        private int EVID = -1;
        private float ObtainedMarks;
        private float totalmarks;

        private void ViewGroupEvaluations_Load(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select DISTINCT Id , Created_On\r\nfrom [dbo].[Group]", con);
            cmd.Connection = con;
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            AllGroups.DataSource = dataTable;
            AllGroups.RowHeadersVisible = true;
            AllGroups.RowHeadersWidth = 30;
        }


        private void refreshAllGroups()
        {
            AllGroups.DataSource = null;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select DISTINCT Id , Created_On\r\nfrom [dbo].[Group]", con);
            cmd.Connection = con;
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            AllGroups.DataSource = dataTable;
        }

        private void refreshAllEvaluations()
        {
            AllEvaluations.DataSource = null;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd1 = new SqlCommand("select E.Name , Ge.ObtainedMarks , E.TotalMarks , E.TotalWeightage\r\nfrom GroupEvaluation as Ge\r\njoin Evaluation as E\r\non E.Id = Ge.EvaluationId\r\nwhere GroupId = @ID", con);
            cmd1.Parameters.AddWithValue("ID", GroupID);
            cmd1.Connection = con;
            SqlDataReader sqlData1 = cmd1.ExecuteReader();
            DataTable dataTable1 = new DataTable();
            dataTable1.Load(sqlData1);
            AllEvaluations.DataSource = dataTable1;
        }

        private void guna2Button4_Click(object sender, EventArgs e)
        {
            refreshAllEvaluations();
        }

        private void guna2Button5_Click(object sender, EventArgs e)
        {
            refreshAllGroups();
        }

        private void guna2Button2_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtGroupSearch.Text))
            {
                BL.Group group = null;
                try
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("select G.Id as GroupID , G.Created_On ,S.Id as StudentID , S.RegistrationNo\r\nfrom [dbo].[Group] as G\r\njoin [dbo].GroupStudent as Gs\r\non Gs.GroupId = G.Id\r\njoin Lookup as L\r\non L.Id = Gs.Status\r\njoin Person as P\r\non P.Id = Gs.StudentId\r\njoin Student as S\r\non S.Id = P.Id\r\nwhere L.Value = 'Active'\r\ngroup by G.Id , G.Created_On , S.RegistrationNo , S.Id\r\n\r\nINTERSECT\r\n\r\nselect top 1 G.Id , G.Created_On ,S.Id , S.RegistrationNo\r\nfrom [dbo].[Group] as G\r\njoin [dbo].GroupStudent as Gs\r\non Gs.GroupId = G.Id\r\njoin Lookup as L\r\non L.Id = Gs.Status\r\njoin Person as P\r\non P.Id = Gs.StudentId\r\njoin Student as S\r\non S.Id = P.Id\r\nwhere L.Value = 'Active'\r\ngroup by G.Id , G.Created_On , S.RegistrationNo , S.Id", con);
                    cmd.Connection = con;
                    SqlDataReader sqlData = cmd.ExecuteReader();
                    DataTable dataTable = new DataTable();
                    dataTable.Load(sqlData);
                    List<BL.Group> list = new List<BL.Group>();
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        if (txtGroupSearch.Text == dataTable.Rows[i]["GroupID"].ToString())
                        {
                            group = new BL.Group(int.Parse(dataTable.Rows[i]["GroupID"].ToString()), dataTable.Rows[i]["Created_On"].ToString(), int.Parse(dataTable.Rows[i]["StudentID"].ToString()), dataTable.Rows[i]["RegistrationNo"].ToString());
                            list.Add(group);
                        }
                    }
                    AllGroups.DataSource = list;
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void txtGroupSearch_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13)
            {
                BL.Group group = null;
                try
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("select G.Id as GroupID , G.Created_On ,S.Id as StudentID , S.RegistrationNo\r\nfrom [dbo].[Group] as G\r\njoin [dbo].GroupStudent as Gs\r\non Gs.GroupId = G.Id\r\njoin Lookup as L\r\non L.Id = Gs.Status\r\njoin Person as P\r\non P.Id = Gs.StudentId\r\njoin Student as S\r\non S.Id = P.Id\r\nwhere L.Value = 'Active'\r\ngroup by G.Id , G.Created_On , S.RegistrationNo , S.Id\r\n\r\nINTERSECT\r\n\r\nselect top 1 G.Id , G.Created_On ,S.Id , S.RegistrationNo\r\nfrom [dbo].[Group] as G\r\njoin [dbo].GroupStudent as Gs\r\non Gs.GroupId = G.Id\r\njoin Lookup as L\r\non L.Id = Gs.Status\r\njoin Person as P\r\non P.Id = Gs.StudentId\r\njoin Student as S\r\non S.Id = P.Id\r\nwhere L.Value = 'Active'\r\ngroup by G.Id , G.Created_On , S.RegistrationNo , S.Id", con);
                    cmd.Connection = con;
                    SqlDataReader sqlData = cmd.ExecuteReader();
                    DataTable dataTable = new DataTable();
                    dataTable.Load(sqlData);
                    List<BL.Group> list = new List<BL.Group>();
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        if (txtGroupSearch.Text == dataTable.Rows[i]["GroupID"].ToString())
                        {
                            group = new BL.Group(int.Parse(dataTable.Rows[i]["GroupID"].ToString()), dataTable.Rows[i]["Created_On"].ToString(), int.Parse(dataTable.Rows[i]["StudentID"].ToString()), dataTable.Rows[i]["RegistrationNo"].ToString());
                            list.Add(group);
                        }
                    }
                    AllGroups.DataSource = list;
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void guna2Button3_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtEvaluationSearch.Text))
            {
                try
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd1 = new SqlCommand("select E.Name , Ge.ObtainedMarks , E.TotalMarks , E.TotalWeightage\r\nfrom GroupEvaluation as Ge\r\njoin Evaluation as E\r\non E.Id = Ge.EvaluationId\r\nwhere GroupId = @ID AND Name = @EVID", con);
                    cmd1.Parameters.AddWithValue("@ID", GroupID);
                    cmd1.Parameters.AddWithValue("@EVID", (txtEvaluationSearch.Text));
                    cmd1.Connection = con;
                    SqlDataReader sqlData1 = cmd1.ExecuteReader();
                    DataTable dataTable1 = new DataTable();
                    dataTable1.Load(sqlData1);
                    AllEvaluations.DataSource = dataTable1;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            
        }

        private void txtEvaluationSearch_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13)
            {
                if (!string.IsNullOrEmpty(txtEvaluationSearch.Text))
                {
                    try
                    {
                        var con = Configuration.getInstance().getConnection();
                        SqlCommand cmd1 = new SqlCommand("select E.Name , Ge.ObtainedMarks , E.TotalMarks , E.TotalWeightage\r\nfrom GroupEvaluation as Ge\r\njoin Evaluation as E\r\non E.Id = Ge.EvaluationId\r\nwhere GroupId = @ID AND Name = @EVID", con);
                        cmd1.Parameters.AddWithValue("@ID", GroupID);
                        cmd1.Parameters.AddWithValue("@EVID", (txtEvaluationSearch.Text));
                        cmd1.Connection = con;
                        SqlDataReader sqlData1 = cmd1.ExecuteReader();
                        DataTable dataTable1 = new DataTable();
                        dataTable1.Load(sqlData1);
                        AllEvaluations.DataSource = dataTable1;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
            }
        }

        private void guna2Button1_Click(object sender, EventArgs e)
        {
            if (EVID != -1)
            {
                Form form = new EditGroupEvaluation(EVID , GroupID , ObtainedMarks , totalmarks);
                form.ShowDialog();
            }
            else
            {
                MessageBox.Show("Please Choose an Item...");
            }
        }

        private void AllGroups_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            GroupID = int.Parse(AllGroups.Rows[e.RowIndex].Cells[0].Value.ToString());
            SqlCommand cmd1 = new SqlCommand("select E.Name , Ge.ObtainedMarks , E.TotalMarks , E.TotalWeightage\r\nfrom GroupEvaluation as Ge\r\njoin Evaluation as E\r\non E.Id = Ge.EvaluationId\r\nwhere GroupId = @ID", con);
            cmd1.Parameters.AddWithValue("ID", GroupID);
            cmd1.Connection = con;
            SqlDataReader sqlData1 = cmd1.ExecuteReader();
            DataTable dataTable1 = new DataTable();
            dataTable1.Load(sqlData1);
            AllEvaluations.DataSource = dataTable1;
            AllEvaluations.RowHeadersVisible = true;
            AllEvaluations.RowHeadersWidth = 30;
        }

        private void AllEvaluations_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            string Name = AllEvaluations.Rows[e.RowIndex].Cells[0].Value.ToString();
            float ObtainedMarks = float.Parse(AllEvaluations.Rows[e.RowIndex].Cells[1].Value.ToString());
            this.ObtainedMarks = ObtainedMarks;
            float TotalMarks = float.Parse(AllEvaluations.Rows[e.RowIndex].Cells[2].Value.ToString());
            totalmarks = TotalMarks;
            float TotalWeightage = float.Parse(AllEvaluations.Rows[e.RowIndex].Cells[3].Value.ToString());
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd1 = new SqlCommand("select Ge.EvaluationId \r\nfrom GroupEvaluation as Ge\r\njoin Evaluation as E\r\non E.Id = Ge.EvaluationId\r\nwhere GroupId = @ID AND Name = @EVID AND Ge.ObtainedMarks = @ObtainedMarks AND E.TotalMarks = @TotalMarks AND E.TotalWeightage = @TotalWeightage", con);
            cmd1.Parameters.AddWithValue("@ID", GroupID);
            cmd1.Parameters.AddWithValue("@ObtainedMarks", ObtainedMarks);
            cmd1.Parameters.AddWithValue("@TotalMarks", TotalMarks);
            cmd1.Parameters.AddWithValue("@TotalWeightage", TotalWeightage);
            cmd1.Parameters.AddWithValue("EVID", Name);
            cmd1.Connection = con;
            SqlDataReader sqlData1 = cmd1.ExecuteReader();
            DataTable dataTable1 = new DataTable();
            dataTable1.Load(sqlData1);
            EVID = int.Parse(dataTable1.Rows[0]["EvaluationId"].ToString());
        }

        private void AllGroups_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            GroupID = int.Parse(AllGroups.Rows[e.RowIndex].Cells[0].Value.ToString());
            SqlCommand cmd1 = new SqlCommand("select E.Name , Ge.ObtainedMarks , E.TotalMarks , E.TotalWeightage\r\nfrom GroupEvaluation as Ge\r\njoin Evaluation as E\r\non E.Id = Ge.EvaluationId\r\nwhere GroupId = @ID", con);
            cmd1.Parameters.AddWithValue("ID", GroupID);
            cmd1.Connection = con;
            SqlDataReader sqlData1 = cmd1.ExecuteReader();
            DataTable dataTable1 = new DataTable();
            dataTable1.Load(sqlData1);
            AllEvaluations.DataSource = dataTable1;
            AllEvaluations.RowHeadersVisible = true;
            AllEvaluations.RowHeadersWidth = 30;
        }

        private void AllEvaluations_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            string Name = AllEvaluations.Rows[e.RowIndex].Cells[0].Value.ToString();
            float ObtainedMarks = float.Parse(AllEvaluations.Rows[e.RowIndex].Cells[1].Value.ToString());
            this.ObtainedMarks = ObtainedMarks;
            float TotalMarks = float.Parse(AllEvaluations.Rows[e.RowIndex].Cells[2].Value.ToString());
            totalmarks = TotalMarks;
            float TotalWeightage = float.Parse(AllEvaluations.Rows[e.RowIndex].Cells[3].Value.ToString());
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd1 = new SqlCommand("select Ge.EvaluationId \r\nfrom GroupEvaluation as Ge\r\njoin Evaluation as E\r\non E.Id = Ge.EvaluationId\r\nwhere GroupId = @ID AND Name = @EVID AND Ge.ObtainedMarks = @ObtainedMarks AND E.TotalMarks = @TotalMarks AND E.TotalWeightage = @TotalWeightage", con);
            cmd1.Parameters.AddWithValue("@ID", GroupID);
            cmd1.Parameters.AddWithValue("@ObtainedMarks", ObtainedMarks);
            cmd1.Parameters.AddWithValue("@TotalMarks", TotalMarks);
            cmd1.Parameters.AddWithValue("@TotalWeightage", TotalWeightage);
            cmd1.Parameters.AddWithValue("EVID", Name);
            cmd1.Connection = con;
            SqlDataReader sqlData1 = cmd1.ExecuteReader();
            DataTable dataTable1 = new DataTable();
            dataTable1.Load(sqlData1);
            EVID = int.Parse(dataTable1.Rows[0]["EvaluationId"].ToString());
        }
    }
}
