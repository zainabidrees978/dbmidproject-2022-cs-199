﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mid_Project_DB.Evaluations
{
    public partial class ViewEvaluations : Form
    {
        private int ID = -1;
        private string name;
        private float totalMarks, totalWeightage;
        public ViewEvaluations()
        {
            InitializeComponent();
        }

        private void ViewEvaluations_Load(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select * from Evaluation");
            cmd.Connection = con;
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            AllEvaluations.DataSource = dataTable;
            AllEvaluations.RowHeadersVisible= true;
            AllEvaluations.RowHeadersWidth = 30;
        }

        private void Add_Evaluation_Click(object sender, EventArgs e)
        {
            Form form = new AddEvaluation();
            form.ShowDialog();
        }

        private void refresh()
        {
            AllEvaluations.DataSource = null;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select * from Evaluation");
            cmd.Connection = con;
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            AllEvaluations.DataSource = dataTable;
        }

        private void refresh_Click(object sender, EventArgs e)
        {
            refresh();
        }

        private void AllEvaluations_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            ID = int.Parse(AllEvaluations.Rows[e.RowIndex].Cells[0].Value.ToString());
            name = AllEvaluations.Rows[e.RowIndex].Cells[1].Value.ToString();
            totalMarks = float.Parse(AllEvaluations.Rows[e.RowIndex].Cells[2].Value.ToString());
            totalWeightage = float.Parse(AllEvaluations.Rows[e.RowIndex].Cells[3].Value.ToString());
        }

        private void Delete_Evaluation_Click(object sender, EventArgs e)
        {
            if (ID != -1)
            {
                try
                {
                    if (MessageBox.Show("Are you sure?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        var con = Configuration.getInstance().getConnection();
                        SqlCommand cmd = new SqlCommand("delete [dbo].[Evaluation] where Id = @ID;");
                        cmd.Parameters.AddWithValue("@ID", ID);
                        cmd.Connection = con;
                        cmd.ExecuteNonQuery();
                        MessageBox.Show("Record Has Been Deleted Successfully...");
                        refresh();
                        ID = -1;
                    }

                }
                catch (Exception ex)
                {

                    MessageBox.Show("You cannot delete this Evaluation...");
                }
            }
            else
            {
                MessageBox.Show("Please select an item to delete...");
            }
        }

        private void Search_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtSearch.Text))
            {
                try
                {
                    AllEvaluations.DataSource = null;
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("select * from [dbo].[Evaluation] where Name = @ID");
                    cmd.Parameters.AddWithValue("@ID", txtSearch.Text);
                    cmd.Connection = con;
                    SqlDataReader sqlData = cmd.ExecuteReader();
                    DataTable dataTable = new DataTable();
                    dataTable.Load(sqlData);
                    AllEvaluations.DataSource = dataTable;
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }

            }
        }

        private void txtSearch_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13)
            {
                try
                {
                    AllEvaluations.DataSource = null;
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("select * from [dbo].[Evaluation] where Name = @ID");
                    cmd.Parameters.AddWithValue("@ID", txtSearch.Text);
                    cmd.Connection = con;
                    SqlDataReader sqlData = cmd.ExecuteReader();
                    DataTable dataTable = new DataTable();
                    dataTable.Load(sqlData);
                    AllEvaluations.DataSource = dataTable;
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void Edit_Evaluation_Click(object sender, EventArgs e)
        {
            if (ID != -1)
            {
                Form form = new EditEvaluation(ID , name , totalMarks , totalWeightage);
                form.ShowDialog();
            }

        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {

        }

        private void AllEvaluations_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            ID = int.Parse(AllEvaluations.Rows[e.RowIndex].Cells[0].Value.ToString());
            name = AllEvaluations.Rows[e.RowIndex].Cells[1].Value.ToString();
            totalMarks = float.Parse(AllEvaluations.Rows[e.RowIndex].Cells[2].Value.ToString());
            totalWeightage = float.Parse(AllEvaluations.Rows[e.RowIndex].Cells[3].Value.ToString());
        }

        private void AllEvaluations_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            ID = int.Parse(AllEvaluations.Rows[e.RowIndex].Cells[0].Value.ToString());
            name = AllEvaluations.Rows[e.RowIndex].Cells[1].Value.ToString();
            totalMarks = float.Parse(AllEvaluations.Rows[e.RowIndex].Cells[2].Value.ToString());
            totalWeightage = float.Parse(AllEvaluations.Rows[e.RowIndex].Cells[3].Value.ToString());
        }

    }
}
