﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mid_Project_DB.Evaluations
{
    public partial class AddEvaluation : Form
    {
        public AddEvaluation()
        {
            InitializeComponent();
        }

        private bool checkID(string s)
        {
            for (int i = 0; i < s.Length; i++)
            {
                if (!Char.IsDigit(s[i]))
                {
                    return false;
                }
            }
            return true;
        }

        private void guna2Button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtName.Text) || string.IsNullOrEmpty(txtMarks.Text) || string.IsNullOrEmpty(txtWeightage.Text))
            {
                MessageBox.Show("Please fill all queries...");
            }
            else
            {
                if (!checkID(txtMarks.Text))
                {
                    errorProvider1.SetError(txtMarks, "Please Enter Only Digits!");
                }
                else if (!checkID(txtWeightage.Text))
                {
                    errorProvider1.SetError(txtWeightage, "Please Enter Only Digits!");
                }
                else
                {
                    if (int.Parse(txtWeightage.Text) + getWeightage() > 100)
                    {
                        MessageBox.Show("Total weightage cannot increase more than 100...\nTotal Weightage Left: " + (100 - getWeightage()));
                    }
                    else
                    {
                        var con = Configuration.getInstance().getConnection();
                        SqlCommand cmd = new SqlCommand("insert into [dbo].[Evaluation] (Name , TotalMarks , TotalWeightage) values (@Name , @TotalMarks , @TotalWeightage)", con);
                        cmd.Parameters.AddWithValue("@Name", txtName.Text);
                        cmd.Parameters.AddWithValue("@TotalMarks", txtMarks.Text);
                        cmd.Parameters.AddWithValue("@TotalWeightage", txtWeightage.Text);
                        cmd.ExecuteNonQuery();
                        MessageBox.Show("Successfully saved");
                        this.Close();
                    }

                }
            }
        }

        /*private float getWeightage()
        {
            float weightage = 0;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select SUM(E.TotalWeightage) as Ev\r\nfrom Evaluation as E", con);
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            weightage =int.Parse( dataTable.Rows[0]["Ev"].ToString());
            return weightage;
        }*/

        private float getWeightage()
        {
            float weightage = 0;

            try
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("select SUM(E.TotalWeightage) as Ev from Evaluation as E", con);
                SqlDataReader sqlData = cmd.ExecuteReader();
                DataTable dataTable = new DataTable();
                dataTable.Load(sqlData);
                weightage = float.Parse(dataTable.Rows[0]["Ev"].ToString());
            }
            catch (Exception ex)
            {
                // Handle the exception here, you might want to log it or display an error message.
                Console.WriteLine("An error occurred: " + ex.Message);
            }

            return weightage;
        }

        private void guna2Button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void AddEvaluation_Load(object sender, EventArgs e)
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            this.CenterToScreen();
        }
    }
}
