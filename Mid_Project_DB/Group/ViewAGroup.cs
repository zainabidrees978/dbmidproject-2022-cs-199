﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mid_Project_DB.Group
{
    public partial class ViewAGroup : Form
    {
        private int ID;
        private string createdOn;
        public ViewAGroup(int GroupID , string Createdon)
        {
            InitializeComponent();
            this.ID = GroupID;
            this.createdOn= Createdon;
        }

        private void guna2Button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ViewAGroup_Load(object sender, EventArgs e)
        {
            GroupID.Text = GroupID.Text + ID;
            CreatedOnLabel.Text = CreatedOnLabel.Text + createdOn;
            this.WindowState= FormWindowState.Maximized;


            //GET STUDENTS
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select A2.RegistrationNo , A2.FirstName , A2.LastName , A2.Contact , A2.Email , A2.Gender , L.Value as Status , A2.AssignmentDate\r\nfrom Lookup as L\r\njoin (select A.RegistrationNo , A.FirstName , A.LastName , A.Contact , A.Email , L.Value as Gender , A.Status , A.AssignmentDate\r\nfrom Lookup as L\r\njoin (select S.RegistrationNo , P.FirstName , P.LastName , P.Contact , P.Email , P.Gender , Gs.Status , Gs.AssignmentDate\r\nfrom [dbo].[Group] as G\r\njoin [dbo].[GroupStudent] as Gs\r\non G.Id = Gs.GroupId\r\njoin Student as S\r\non S.Id = Gs.StudentId\r\njoin Person as P\r\non P.Id = S.Id\r\nwhere G.Id = @ID\r\ngroup by S.RegistrationNo , P.FirstName , P.LastName , P.Contact , P.Email , P.Gender , Gs.Status , Gs.AssignmentDate) as A\r\non A.Gender = L.Id) as A2\r\non A2.Status = L.Id", con);
            cmd.Parameters.AddWithValue("@ID" , ID);
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            AllStudents.DataSource = dataTable;

            //GET PROJECT INFORMATION
            SqlCommand cmd1 = new SqlCommand("select P.Id, P.Title , P.Description\r\nfrom [dbo].[Group] as G\r\njoin GroupProject as Gp\r\non G.Id = Gp.GroupId\r\njoin Project as P\r\non P.Id = Gp.ProjectId\r\nwhere G.Id = @ID", con);
            cmd1.Parameters.AddWithValue("@ID", ID);
            SqlDataReader sqlData1 = cmd1.ExecuteReader();
            DataTable dataTable1 = new DataTable();
            dataTable1.Load(sqlData1);
            BL.Project project = new BL.Project(dataTable1.Rows[0]["Title"].ToString() , dataTable1.Rows[0]["Description"].ToString() , int.Parse(dataTable1.Rows[0]["Id"].ToString()));
            txtTitle.Text = project.Title;
            txtDescription.Text = project.Description;
            ProjectView.Text = ProjectView.Text + project.ID;


            //GET ADVISRORS INFORMATION
            SqlCommand cmd2 = new SqlCommand("select P.FirstName , P.LastName, A1.Value as Role\r\nfrom Person as P\r\njoin (select A.AdvisorId , A.AdvisorRole , L.Value\r\nfrom Lookup as L\r\njoin (select Pa.AdvisorId , Pa.AdvisorRole\r\nfrom [dbo].[Group] as G\r\njoin GroupProject as Gp\r\non G.Id = Gp.GroupId\r\njoin Project as P\r\non P.Id = Gp.ProjectId\r\njoin ProjectAdvisor as Pa\r\non Pa.ProjectId = P.Id\r\nwhere G.Id = @ID) as A\r\non A.AdvisorRole = L.Id) as A1\r\non A1.AdvisorId = P.Id", con);
            cmd2.Parameters.AddWithValue("@ID", ID);
            SqlDataReader sqlData2 = cmd2.ExecuteReader();
            DataTable dataTable2 = new DataTable();
            dataTable2.Load(sqlData2);
            List<BL.Person> people = new List<BL.Person>();
            for (int i = 0; i < dataTable2.Rows.Count; i++)
            {
                BL.Person person = new BL.Person(dataTable2.Rows[i]["Role"].ToString(), dataTable2.Rows[i]["FirstName"].ToString() , dataTable2.Rows[i]["LastName"].ToString());
                people.Add(person);
            }
            Advisror1.Text = people[0].Role1;
            Advisror1Name.Text = people[0].FirstName1 + " " + people[0].LastName1;

            Advisror2.Text = people[1].Role1;
            Advisror2name.Text = people[1].FirstName1 +" "+ people[1].LastName1;

            Advisror3.Text = people[2].Role1;
            Advisror3Name.Text = people[2].FirstName1 + " " + people[2].LastName1;

            //EVALUATIONS INFORMATION
            SqlCommand cmd3 = new SqlCommand("select  E.Name , E.TotalMarks , E.TotalWeightage\r\nfrom Evaluation as E\r\njoin GroupEvaluation as Ge\r\non Ge.EvaluationId = E.Id\r\njoin [dbo].[Group] as G\r\non G.Id = Ge.GroupId\r\nwhere G.Id = @ID", con);
            cmd3.Parameters.AddWithValue("@ID", ID);
            SqlDataReader sqlData3 = cmd3.ExecuteReader();
            DataTable dataTable3 = new DataTable();
            dataTable3.Load(sqlData3);
            AllEvaluations.DataSource = dataTable3;
        }

        private void AllStudents_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            ID = int.Parse(AllStudents.Rows[e.RowIndex].Cells[0].Value.ToString());
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select  E.Name , E.TotalMarks , E.TotalWeightage\r\nfrom Evaluation as E\r\njoin GroupEvaluation as Ge\r\non Ge.EvaluationId = E.Id\r\njoin [dbo].[Group] as G\r\non G.Id = Ge.GroupId\r\nwhere G.Id = @ID", con);
            cmd.Parameters.AddWithValue("@ID", ID);
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            AllEvaluations.DataSource = dataTable;
        }
    }
}
