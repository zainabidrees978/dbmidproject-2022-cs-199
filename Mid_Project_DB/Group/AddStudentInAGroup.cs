﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mid_Project_DB.Group
{
    public partial class AddStudentInAGroup : Form
    {
        private int GroupId = -1;
        private int StudentUD = -1;
        private DataTable students = new DataTable();
        BL.Student student;
        BL.Student selectedStudent;
        private int SelectedStudentID = -1;
        public AddStudentInAGroup()
        {
            InitializeComponent();
        }

        private void AddStudentInAGroup_Load(object sender, EventArgs e)
        {
            //ALL GROUPS

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select *\r\nfrom dbo.[Group]", con);
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            AllGroups.DataSource = dataTable;
            AllGroups.RowHeadersVisible = true;
            AllGroups.RowHeadersWidth = 30;

            //ALL STUDENTS
            SqlCommand cmd1 = new SqlCommand("select S.Id, S.RegistrationNo , FirstName , LastName , Contact , Email\r\nfrom Student as S\r\njoin Person as P\r\non S.Id = P.Id EXCEPT select S.Id , S.RegistrationNo , P.FirstName , P.LastName , P.Contact , P.Email\r\nfrom [dbo].[Group] as G\r\njoin [dbo].GroupStudent as Gs\r\non Gs.GroupId = G.Id\r\njoin Lookup as L\r\non L.Id = Gs.Status\r\njoin Person as P\r\non P.Id = Gs.StudentId\r\njoin Student as S\r\non S.Id = P.Id\r\nwhere L.Value = 'Active'", con);
            cmd1.Connection = con;
            SqlDataReader sqlData1 = cmd1.ExecuteReader();
            DataTable dataTable1 = new DataTable();
/*            dataTable1.Load(sqlData1);*/
            students.Load(sqlData1);
            AllStudents.DataSource = students;
            AllStudents.RowHeadersVisible = true;
            AllStudents.RowHeadersWidth =30;
        }


        private bool checkID(string s)
        {
            for (int i = 0; i < s.Length; i++)
            {
                if (!Char.IsDigit(s[i]))
                {
                    return false;
                }
            }
            return true;
        }


        private void guna2Button4_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtGroupSearch.Text))
            {
                if (checkID(txtGroupSearch.Text))
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("select *\r\nfrom dbo.[Group] where Id = @ID", con);
                    cmd.Parameters.AddWithValue("@ID", int.Parse(txtGroupSearch.Text));
                    SqlDataReader sqlData = cmd.ExecuteReader();
                    DataTable dataTable = new DataTable();
                    dataTable.Load(sqlData);
                    AllGroups.DataSource = dataTable;
                }
                else
                {
                    errorProvider1.SetError(txtGroupSearch, "Can only accept numbers!");

                }
            }
            else
            {
                errorProvider1.SetError(txtGroupSearch, "Please fill this query!");
            }
        }

        private void txtGroupSearch_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13)
            {
                if (!string.IsNullOrEmpty(txtGroupSearch.Text))
                {
                    if (checkID(txtGroupSearch.Text))
                    {
                        var con = Configuration.getInstance().getConnection();
                        SqlCommand cmd = new SqlCommand("select *\r\nfrom dbo.[Group] where Id = @ID", con);
                        cmd.Parameters.AddWithValue("@ID" , int.Parse(txtGroupSearch.Text));
                        SqlDataReader sqlData = cmd.ExecuteReader();
                        DataTable dataTable = new DataTable();
                        dataTable.Load(sqlData);
                        AllGroups.DataSource = dataTable;
                    }
                    else
                    {
                        errorProvider1.SetError(txtGroupSearch, "Can only accept numbers!");

                    }
                }
                else
                {
                    errorProvider1.SetError(txtGroupSearch , "Please fill this query!");
                }
            }
        }

        private void guna2Button3_Click(object sender, EventArgs e)
        {
            AllGroupsRefresh();

        }

        private void AllGroupsRefresh()
        {
            AllGroups.DataSource = null;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select *\r\nfrom dbo.[Group]", con);
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            AllGroups.DataSource = dataTable;
        }

        private void refreshAllStudents()
        {
            var con = Configuration.getInstance().getConnection();
            AllStudents.DataSource= null;
            SqlCommand cmd1 = new SqlCommand("select S.Id, S.RegistrationNo , FirstName , LastName , Contact , Email\r\nfrom Student as S\r\njoin Person as P\r\non S.Id = P.Id EXCEPT select S.Id , S.RegistrationNo , P.FirstName , P.LastName , P.Contact , P.Email\r\nfrom [dbo].[Group] as G\r\njoin [dbo].GroupStudent as Gs\r\non Gs.GroupId = G.Id\r\njoin Lookup as L\r\non L.Id = Gs.Status\r\njoin Person as P\r\non P.Id = Gs.StudentId\r\njoin Student as S\r\non S.Id = P.Id\r\nwhere L.Value = 'Active'", con);
            cmd1.Connection = con;
            SqlDataReader sqlData1 = cmd1.ExecuteReader();
            DataTable dataTable1 = new DataTable();
            dataTable1.Load(sqlData1);
            students.Load(sqlData1);
            AllStudents.DataSource = dataTable1;
        }

        private void guna2Button5_Click(object sender, EventArgs e)
        {
            refreshAllStudents();
        }

        private void guna2Button6_Click(object sender, EventArgs e)
        {
            bool flag = false;
            if (!string.IsNullOrEmpty(txtStudentSearch.Text))
            {
                for (int i = 0; i < students.Rows.Count; i++)
                {
                    if ((txtStudentSearch.Text) == (students.Rows[i]["RegistrationNo"].ToString()))
                    {
                        student = new BL.Student(int.Parse(students.Rows[i]["Id"].ToString()), students.Rows[i]["RegistrationNo"].ToString(), students.Rows[i]["FirstName"].ToString(), students.Rows[i]["LastName"].ToString(), students.Rows[i]["Contact"].ToString(), students.Rows[i]["Email"].ToString());
                        List<BL.Student> studentss = new List<BL.Student>();
                        studentss.Add(student);
                        AllStudents.DataSource = studentss;
                        flag = true;
                        break;
                    }
                }
                if (!flag)
                {
                    AllStudents.DataSource = null;
                }
            }
            else
            {
                errorProvider1.SetError(txtStudentSearch, "Please fill this query!");
            }
        }

        private void txtStudentSearch_KeyUp(object sender, KeyEventArgs e)
        {
            bool flag = false;
            if (e.KeyValue == 13)
            {
                if (!string.IsNullOrEmpty(txtStudentSearch.Text))
                {
                    for (int i = 0; i < students.Rows.Count; i++)
                    {
                        if ((txtStudentSearch.Text) == (students.Rows[i]["RegistrationNo"].ToString()))
                        {
                            student = new BL.Student(int.Parse(students.Rows[i]["Id"].ToString()) , students.Rows[i]["RegistrationNo"].ToString() , students.Rows[i]["FirstName"].ToString() , students.Rows[i]["LastName"].ToString() , students.Rows[i]["Contact"].ToString() , students.Rows[i]["Email"].ToString());
                            List<BL.Student> studentss = new List<BL.Student>();
                            studentss.Add(student);
                            AllStudents.DataSource = studentss;
                            flag = true;
                            break;
                        }
                    }
                    if (!flag)
                    {
                        AllStudents.DataSource = null;
                    }
                }
                else
                {
                    errorProvider1.SetError(txtStudentSearch, "Please fill this query!");
                }
            } 

        }


        private void guna2Button1_Click(object sender, EventArgs e)
        {
            if (GroupId != -1)
            {
                if (StudentUD !=-1)
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("select COUNT(Gs.Status) as Active\r\nfrom [dbo].[Group] as G\r\njoin GroupStudent as Gs\r\non G.Id = Gs.GroupId\r\njoin Lookup as L\r\non L.Id = Gs.Status\r\nwhere G.Id = @ID AND Gs.Status = 3", con);
                    cmd.Parameters.AddWithValue("@ID" , GroupId);
                    SqlDataReader sqlData = cmd.ExecuteReader();
                    DataTable dataTable = new DataTable();
                    dataTable.Load(sqlData);
                    if (int.Parse(dataTable.Rows[0]["Active"].ToString()) < 3)
                    {
                        if (checkStudentisPresent())
                        {
                            SqlCommand studentGroup = new SqlCommand("update GroupStudent set Status = 3 where GroupId = @GroupID AND StudentId = @SID", con);
                            studentGroup.Parameters.AddWithValue("@Date", DateTime.Now.ToString("yyyy-MM-dd"));
                            studentGroup.Parameters.AddWithValue("@SID", StudentUD);
                            studentGroup.Parameters.AddWithValue("@GroupID", GroupId);
                            studentGroup.ExecuteNonQuery();
                            MessageBox.Show("Student Added Successfully...");
                        }
                        else
                        {
                            SqlCommand studentGroup = new SqlCommand("insert into [dbo].[GroupStudent](GroupId , StudentId , Status ,AssignmentDate) values (@GroupID , @ID , 3, @Date)", con);
                            studentGroup.Parameters.AddWithValue("@Date", DateTime.Now.ToString("yyyy-MM-dd"));
                            studentGroup.Parameters.AddWithValue("@ID", StudentUD);
                            studentGroup.Parameters.AddWithValue("@GroupID", GroupId);
                            studentGroup.ExecuteNonQuery();
                            MessageBox.Show("Student Added Successfully...");
                        }

                    }
                    else
                    {
                        MessageBox.Show("Group cannot have more than 3 Active students...");
                    }
                }
                else
                {
                    MessageBox.Show("Please Select A Student...");
                }
            }
            else
            {
                MessageBox.Show("Please Select A Group...");
            }
            refreshAllStudents();
            AllGroupsRefresh();
            refreshSelectedStudents();
        }

        private bool checkStudentisPresent()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select COUNT(Gs.Status) as Active\r\nfrom [dbo].[Group] as G\r\njoin GroupStudent as Gs\r\non G.Id = Gs.GroupId\r\njoin Lookup as L\r\non L.Id = Gs.Status\r\nwhere G.Id = @ID AND Gs.StudentId = @SID", con);
            cmd.Parameters.AddWithValue("@ID", GroupId);
            cmd.Parameters.AddWithValue("@SID", StudentUD);
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            if (int.Parse(dataTable.Rows[0]["Active"].ToString()) >= 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void guna2Button2_Click(object sender, EventArgs e)
        {
            if (SelectedStudentID != -1)
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("update GroupStudent set Status = 4 where GroupId = @ID AND StudentId = @SID", con);
                cmd.Parameters.AddWithValue("@ID", GroupId);
                cmd.Parameters.AddWithValue("@SID", SelectedStudentID);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Student has been removed...");
                refreshSelectedStudents();
                refreshAllStudents();
                SelectedStudentID = -1;
            }
            else
            {
                MessageBox.Show("Please select a student to remove...");
            }
        }

        private void refreshSelectedStudents()
        {
            SelectedGroup.DataSource = null;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select S.Id, S.RegistrationNo , P.FirstName , P.LastName ,L2.Value as Gender, P.Contact  , P.Email,  L.Value as Status , Gs.AssignmentDate\r\nfrom [dbo].[Group] as G\r\njoin GroupStudent as Gs\r\non G.Id = Gs.GroupId\r\njoin Lookup as L\r\non L.Id = Gs.Status\r\njoin Person as P\r\non P.Id = Gs.StudentId\r\njoin Student as S\r\non S.Id = Gs.StudentId\r\njoin Lookup as L2\r\non L2.Id = P.Gender\r\nwhere G.Id = @ID", con);
            cmd.Parameters.AddWithValue("@ID", GroupId);
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            SelectedGroup.DataSource = dataTable;
            SelectedGroup.RowHeadersVisible = true;
            SelectedGroup.RowHeadersWidth = 30;
        }

        private void AllStudents_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            StudentUD = int.Parse(AllStudents.Rows[e.RowIndex].Cells[0].Value.ToString());
            string registrationNo1 = AllStudents.Rows[e.RowIndex].Cells[1].Value.ToString();
            string firstname1 = AllStudents.Rows[e.RowIndex].Cells[2].Value.ToString();
            string lastname1 = AllStudents.Rows[e.RowIndex].Cells[3].Value.ToString();
            string contact1 = AllStudents.Rows[e.RowIndex].Cells[4].Value.ToString();
            string email1 = AllStudents.Rows[e.RowIndex].Cells[5].Value.ToString();
            student = new BL.Student(StudentUD, registrationNo1, firstname1, lastname1, contact1, email1);
        }

        private void SelectedGroup_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            SelectedStudentID = int.Parse(SelectedGroup.Rows[e.RowIndex].Cells[0].Value.ToString());
            string registrationNo1 = SelectedGroup.Rows[e.RowIndex].Cells[1].Value.ToString();
            string firstname1 = SelectedGroup.Rows[e.RowIndex].Cells[2].Value.ToString();
            string lastname1 = SelectedGroup.Rows[e.RowIndex].Cells[3].Value.ToString();
            string contact1 = SelectedGroup.Rows[e.RowIndex].Cells[4].Value.ToString();
            string email1 = SelectedGroup.Rows[e.RowIndex].Cells[5].Value.ToString();
        }

        private void AllGroups_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            GroupId = int.Parse(AllGroups.Rows[e.RowIndex].Cells[0].Value.ToString());
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select S.Id, S.RegistrationNo , P.FirstName , P.LastName ,L2.Value as Gender, P.Contact  , P.Email,  L.Value as Status , Gs.AssignmentDate\r\nfrom [dbo].[Group] as G\r\njoin GroupStudent as Gs\r\non G.Id = Gs.GroupId\r\njoin Lookup as L\r\non L.Id = Gs.Status\r\njoin Person as P\r\non P.Id = Gs.StudentId\r\njoin Student as S\r\non S.Id = Gs.StudentId\r\njoin Lookup as L2\r\non L2.Id = P.Gender\r\nwhere G.Id = @ID", con);
            cmd.Parameters.AddWithValue("@ID", GroupId);
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            refreshSelectedStudents();
        }

        private void AllGroups_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            GroupId = int.Parse(AllGroups.Rows[e.RowIndex].Cells[0].Value.ToString());
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select S.Id, S.RegistrationNo , P.FirstName , P.LastName ,L2.Value as Gender, P.Contact  , P.Email,  L.Value as Status , Gs.AssignmentDate\r\nfrom [dbo].[Group] as G\r\njoin GroupStudent as Gs\r\non G.Id = Gs.GroupId\r\njoin Lookup as L\r\non L.Id = Gs.Status\r\njoin Person as P\r\non P.Id = Gs.StudentId\r\njoin Student as S\r\non S.Id = Gs.StudentId\r\njoin Lookup as L2\r\non L2.Id = P.Gender\r\nwhere G.Id = @ID", con);
            cmd.Parameters.AddWithValue("@ID", GroupId);
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            refreshSelectedStudents();
        }

        private void SelectedGroup_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            SelectedStudentID = int.Parse(SelectedGroup.Rows[e.RowIndex].Cells[0].Value.ToString());
            string registrationNo1 = SelectedGroup.Rows[e.RowIndex].Cells[1].Value.ToString();
            string firstname1 = SelectedGroup.Rows[e.RowIndex].Cells[2].Value.ToString();
            string lastname1 = SelectedGroup.Rows[e.RowIndex].Cells[3].Value.ToString();
            string contact1 = SelectedGroup.Rows[e.RowIndex].Cells[4].Value.ToString();
            string email1 = SelectedGroup.Rows[e.RowIndex].Cells[5].Value.ToString();
        }

        private void AllStudents_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            StudentUD = int.Parse(AllStudents.Rows[e.RowIndex].Cells[0].Value.ToString());
            string registrationNo1 = AllStudents.Rows[e.RowIndex].Cells[1].Value.ToString();
            string firstname1 = AllStudents.Rows[e.RowIndex].Cells[2].Value.ToString();
            string lastname1 = AllStudents.Rows[e.RowIndex].Cells[3].Value.ToString();
            string contact1 = AllStudents.Rows[e.RowIndex].Cells[4].Value.ToString();
            string email1 = AllStudents.Rows[e.RowIndex].Cells[5].Value.ToString();
            student = new BL.Student(StudentUD, registrationNo1, firstname1, lastname1, contact1, email1);
        }
    }
}
