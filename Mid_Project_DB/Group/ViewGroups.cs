﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mid_Project_DB.Group
{
    public partial class ViewGroups : Form
    {
        private int ID = -1;
        private string createdOn;
        public ViewGroups()
        {
            InitializeComponent();
        }

        private void ViewGroups_Load(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select * from [dbo].[Group]");
            cmd.Connection = con;
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            bunifuDataGridView1.DataSource = dataTable;
            bunifuDataGridView1.RowHeadersVisible = true;
            bunifuDataGridView1.RowHeadersWidth = 30;
/*            bunifuDataGridView1.RowHeadersDefaultCellStyle = bunifuDataGridView1.DefaultCellStyle;
*/        }



        private void Add_Advisor_Click(object sender, EventArgs e)
        {
            if (ID != -1)
            {
                Form form = new ViewAGroup(ID, createdOn);
                form.ShowDialog();
            }
            else
            {
                MessageBox.Show("Please Select A Group to view...");
            }
        }


        private void Delete_Advisor_Click(object sender, EventArgs e)
        {
            if (ID!= -1)
            {
                if (MessageBox.Show("Are you sure?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("delete GroupStudent where GroupId = @ID\r\ndelete GroupEvaluation where GroupId = @ID\r\ndelete GroupProject where GroupId = @ID\r\ndelete ProjectAdvisor where ProjectId = @PID\r\ndelete dbo.[Group] where Id = @ID");
                    cmd.Connection = con;
                    cmd.Parameters.AddWithValue("@ID", ID);
                    cmd.Parameters.AddWithValue("@PID", getProjectID());
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Group deleted successfully...");
                    refresh();
                }

            }
            else
            {
                MessageBox.Show("Please select a group to delte...");
            }
        }

        private int getProjectID()
        {
            int IDs;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select DISTINCT Pa.ProjectId as ID\r\nfrom ProjectAdvisor as Pa\r\njoin GroupProject as Gp\r\non Gp.ProjectId = Pa.ProjectId\r\njoin dbo.[Group] as G\r\non G.Id = Gp.GroupId\r\nwhere G.Id = @ID");
            cmd.Connection = con;
            cmd.Parameters.AddWithValue("@ID" , ID);
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            IDs = int.Parse(dataTable.Rows[0]["ID"].ToString());
            return IDs;
        }

        private void guna2Button4_Click(object sender, EventArgs e)
        {
            refresh();
        }

        private void refresh()
        {
            bunifuDataGridView1.DataSource = null;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select * from [dbo].[Group]");
            cmd.Connection = con;
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            bunifuDataGridView1.DataSource = dataTable;
        }

        private void bunifuDataGridView1_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {

        }

        private void bunifuDataGridView1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            ID = int.Parse(bunifuDataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString());
            createdOn = bunifuDataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
        }

        private void bunifuDataGridView1_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            ID = int.Parse(bunifuDataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString());
            createdOn = bunifuDataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
        }
    }
}
