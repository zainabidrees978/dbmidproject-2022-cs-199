﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Mid_Project_DB.BL;

namespace Mid_Project_DB.Group
{
    public partial class AddGroup : Form
    {
        private List<BL.Student> students = new List<BL.Student>();
        private List<BL.Project> projects= new List<BL.Project>();
        private string registrationNumber;
        private string Firstname, Lastname, Contact, Email;
        private BL.Student student=null;
        private string Title, Description;
        private int ID;
        private BL.Project project = null;
        private int selectedStudentID = -1;
        public AddGroup()
        {
            InitializeComponent();
        }

        private void AddGroup_Load(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select S.Id, S.RegistrationNo , FirstName , LastName , Contact , Email\r\nfrom Student as S\r\njoin Person as P\r\non S.Id = P.Id EXCEPT select S.Id , S.RegistrationNo , P.FirstName , P.LastName , P.Contact , P.Email\r\nfrom [dbo].[Group] as G\r\njoin [dbo].GroupStudent as Gs\r\non Gs.GroupId = G.Id\r\njoin Lookup as L\r\non L.Id = Gs.Status\r\njoin Person as P\r\non P.Id = Gs.StudentId\r\njoin Student as S\r\non S.Id = P.Id\r\nwhere L.Value = 'Active'", con);
            cmd.Connection = con;
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            AllStudents.DataSource = dataTable;
            AllStudents.RowHeadersVisible = true;   
            AllStudents.RowHeadersWidth = 30;

            //PROJECT DATA 
            SqlCommand cmd1 = new SqlCommand("select Id , Title , Description from Project EXCEPT select P.Id , P.Title , P.Description\r\nfrom [dbo].[Group] as G\r\njoin GroupProject as Gp\r\non G.Id = Gp.GroupId\r\njoin Project as P\r\non P.Id = Gp.ProjectId", con);
            cmd1.Connection = con;
            SqlDataReader sqlData1 = cmd1.ExecuteReader();
            DataTable dataTable1 = new DataTable();
            dataTable1.Load(sqlData1);
            AllProjects.DataSource = dataTable1;
            AllProjects.RowHeadersVisible = true;   
            AllProjects.RowHeadersWidth = 30;


            //Main Advisor Combo box
            SqlCommand cmd2 = new SqlCommand("select FirstName\r\nfrom Advisor as A\r\njoin Person as P\r\non P.Id = A.Id", con);
            cmd2.Connection = con;
            SqlDataReader sqlData2 = cmd2.ExecuteReader();
            DataTable dataTable2 = new DataTable();
            dataTable2.Load(sqlData2);
            SqlCommand cmd3 = new SqlCommand("select LastName\r\nfrom Advisor as A\r\njoin Person as P\r\non P.Id = A.Id", con);
            cmd3.Connection = con;
            SqlDataReader sqlData3 = cmd3.ExecuteReader();
            DataTable dataTable3 = new DataTable();
            dataTable3.Load(sqlData3);
            List<string> list1 = new List<string>();
            List<string> list2 = new List<string>();
            List<string> list3 = new List<string>();
            for (int i = 0; i < dataTable2.Rows.Count; i++)
            {
                string s = dataTable2.Rows[i]["FirstName"].ToString() +" "+ dataTable3.Rows[i]["LastName"].ToString();
                list1.Add(s);
                list2.Add(s);
                list3.Add(s);
            }
            MainAdvisor.DataSource = list1;
            CoAdvisor.DataSource = list2;
            IndustryAdvisor.DataSource = list3;
        }

        private void refresh()
        {
            SelectedStudents.DataSource = null;
            SelectedStudents.Refresh();
            SelectedStudents.DataSource = students;
            SelectedStudents.RowHeadersVisible = true;
            SelectedStudents.RowHeadersWidth = 30;
            SelectedStudents.Refresh();
        }

        private void refreshAllStudents()
        {
            AllStudents.DataSource = null;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select S.Id, S.RegistrationNo , FirstName , LastName , Contact , Email\r\nfrom Student as S\r\njoin Person as P\r\non S.Id = P.Id EXCEPT select S.Id , S.RegistrationNo , P.FirstName , P.LastName , P.Contact , P.Email\r\nfrom [dbo].[Group] as G\r\njoin [dbo].GroupStudent as Gs\r\non Gs.GroupId = G.Id\r\njoin Lookup as L\r\non L.Id = Gs.Status\r\njoin Person as P\r\non P.Id = Gs.StudentId\r\njoin Student as S\r\non S.Id = P.Id\r\nwhere L.Value = 'Active'", con);
            cmd.Connection = con;
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            AllStudents.DataSource = dataTable;
        }

        private void tableLayoutPanel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void toolStripStatusLabel1_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }


        private void guna2Button1_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < students.Count; i++)
            {
                if (students[i].Firstname == Firstname && students[i].RegistrationNo == registrationNumber && students[i].ID == selectedStudentID)
                {
                    students.RemoveAt(i);
                }
            }
            refresh();
        }

        private void tableLayoutPanel5_Paint(object sender, PaintEventArgs e)
        {

        }

        private void StudentSearch_Click(object sender, EventArgs e)
        {
            try
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("select S.Id, S.RegistrationNo , FirstName , LastName , Contact , Email\r\nfrom Student as S\r\njoin Person as P\r\non S.Id = P.Id where S.RegistrationNo = '" + txtStudentSearch.Text + "'", con   );
                cmd.Connection = con;
                SqlDataReader sqlData = cmd.ExecuteReader();
                DataTable dataTable = new DataTable();
                dataTable.Load(sqlData);
                AllStudents.DataSource = dataTable;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void txtStudentSearch_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13)
            {
                try
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("select S.Id, S.RegistrationNo , FirstName , LastName , Contact , Email\r\nfrom Student as S\r\njoin Person as P\r\non S.Id = P.Id where S.RegistrationNo = '" + txtStudentSearch.Text + "'", con   );
                    cmd.Connection = con;
                    SqlDataReader sqlData = cmd.ExecuteReader();
                    DataTable dataTable = new DataTable();
                    dataTable.Load(sqlData);
                    AllStudents.DataSource = dataTable;
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }



        private void refreshSelectedProjects()
        {
            SelectedProjects.DataSource = null;
            SelectedProjects.DataSource = projects;
        }

        private void ProjectDelete_Click(object sender, EventArgs e)
        {
            if (projects.Count != 0)
            {
                projects.RemoveAt(0);
                refreshSelectedProjects();
            }

        }

        private void ProjectSearch_Click(object sender, EventArgs e)
        {
            if (txtProjectSearch.Text == "")
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd1 = new SqlCommand("select Id , Title , Description from Project", con);
                cmd1.Connection = con;
                SqlDataReader sqlData1 = cmd1.ExecuteReader();
                DataTable dataTable1 = new DataTable();
                dataTable1.Load(sqlData1);
                AllProjects.DataSource = dataTable1;
            }
            else
            {
                try
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("select Id , Title , Description from Project where Title = '" + txtProjectSearch.Text + "'", con   );
                    cmd.Connection = con;
                    SqlDataReader sqlData = cmd.ExecuteReader();
                    DataTable dataTable = new DataTable();
                    dataTable.Load(sqlData);
                    AllProjects.DataSource = dataTable;
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
            
        }

        private void txtProjectSearch_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13)
            {
                try
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("select Id , Title , Description from Project where Title = '" + txtProjectSearch.Text + "'", con);
                    cmd.Connection = con;
                    SqlDataReader sqlData = cmd.ExecuteReader();
                    DataTable dataTable = new DataTable();
                    dataTable.Load(sqlData);
                    AllProjects.DataSource = dataTable;
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void guna2Button3_Click(object sender, EventArgs e)
        {
            refreshAllStudents();
        }

        private void guna2Button4_Click(object sender, EventArgs e)
        {
            refreshAllProjects();
        }

        private void refreshAllProjects()
        {
            var con = Configuration.getInstance().getConnection();
            AllProjects.DataSource = null;
            SqlCommand cmd1 = new SqlCommand("select Id , Title , Description\r\nfrom Project\r\n\r\nEXCEPT \r\n\r\nselect P.Id , P.Title , P.Description\r\nfrom GroupProject as Gp\r\njoin Project as P\r\non P.Id = Gp.ProjectId", con    );
            cmd1.Connection = con;
            SqlDataReader sqlData1 = cmd1.ExecuteReader();
            DataTable dataTable1 = new DataTable();
            dataTable1.Load(sqlData1);
            AllProjects.DataSource = dataTable1;
        }

        private int getID(string FirstName , string LastName)
        {
            int ID;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select DISTINCT A.Id as ID \r\nfrom [dbo].[Advisor] as A\r\njoin Person as P\r\non P.Id = A.Id\r\nwhere P.FirstName =@FirstName AND P.LastName =@LastName", con);
            cmd.Parameters.AddWithValue("@FirstName", FirstName);
            cmd.Parameters.AddWithValue("@LastName", LastName);
            cmd.Connection = con;
            SqlDataReader sqlData3 = cmd.ExecuteReader();
            DataTable dataTable3 = new DataTable();
            dataTable3.Load(sqlData3);
            List<int> list1 = new List<int>();
            for (int i = 0; i < dataTable3.Rows.Count; i++)
            {
                string s = dataTable3.Rows[i]["ID"].ToString();
                list1.Add(int.Parse(s));
            }
            return list1[0];
        }

        private string getFirstName(string name)
        {
            string FirstName = "";
            int count = 0;  
            while (name[count] != ' ')
            {
                FirstName += name[count];   
                count++;
            }
            return FirstName;
        }

        private string getLastName(string name)
        {
            string LastName = "";
            int count = 0;
            while (name[count] != ' ')
            {
                count++;
            }
            count++;
            for (int i = count; i < name.Length; i++)
            {
                LastName += name[i];
            }
            return LastName;    
        }

        private int getAdvisorRole(string role)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select Id as ID\r\nfrom [dbo].[Lookup] \r\nwhere Value = @Role", con);
            cmd.Parameters.AddWithValue("@Role", role);
            SqlDataReader sqlData3 = cmd.ExecuteReader();
            DataTable dataTable3 = new DataTable();
            dataTable3.Load(sqlData3);
            List<int> list1 = new List<int>();
            for (int i = 0; i < dataTable3.Rows.Count; i++)
            {
                string s = dataTable3.Rows[i]["ID"].ToString();
                list1.Add(int.Parse(s));
            }
            return list1[0];
        }
        private void guna2Button2_Click(object sender, EventArgs e)
        {

            if (MainAdvisor.SelectedIndex == CoAdvisor.SelectedIndex || MainAdvisor.SelectedIndex == IndustryAdvisor.SelectedIndex)
            {
                MessageBox.Show("An Advisor cannot have more than two roles");
            }
            else if (projects.Count == 0)
            {
                MessageBox.Show("Please Select a Project");
            }
            else if (students.Count == 0)
            {
                MessageBox.Show("Please Select a Student");
            }
            else
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand group = new SqlCommand("insert into [dbo].[Group](Created_On) values (@Date)" , con);
                group.Parameters.AddWithValue("@Date", DateTime.Now.ToString("yyyy-MM-dd"));
                group.ExecuteNonQuery();

                for (int i = 0; i < students.Count; i++)
                {
                    SqlCommand studentGroup = new SqlCommand("insert into [dbo].[GroupStudent](GroupId , StudentId , Status ,AssignmentDate) values ((select TOP 1 Id from  [dbo].[Group] ORDER BY Id DESC) , @ID , 3, @Date)", con);
                    studentGroup.Parameters.AddWithValue("@Date", DateTime.Now.ToString("yyyy-MM-dd"));
                    studentGroup.Parameters.AddWithValue("@ID" , students[i].ID);
                    studentGroup.ExecuteNonQuery();
                }

                SqlCommand ProjectGroup = new SqlCommand("insert into [dbo].[GroupProject](GroupId , ProjectId , AssignmentDate) values ((select TOP 1 Id from  [dbo].[Group] ORDER BY Id DESC) , @ProjectId , @Date)\r\n", con);
                ProjectGroup.Parameters.AddWithValue("@ProjectId", projects[0].ID);
                ProjectGroup.Parameters.AddWithValue("@Date", DateTime.Now.ToString("yyyy-MM-dd"));
                ProjectGroup.ExecuteNonQuery();

                SqlCommand ProjectAdvisor = new SqlCommand("insert into [dbo].[ProjectAdvisor](AdvisorId , ProjectId , AdvisorRole , AssignmentDate) values (@AdvisorId , @ProjectId , @AdvisorRole , @Date)\r\n", con);
                int ID1 = getID(getFirstName(MainAdvisor.SelectedItem.ToString()), getLastName(MainAdvisor.SelectedItem.ToString()));
                int ID2 = projects[0].ID;
                int AdvisorRole = getAdvisorRole("Main Advisor");
                ProjectAdvisor.Parameters.AddWithValue("@AdvisorId", getID(getFirstName(MainAdvisor.SelectedItem.ToString()), getLastName(MainAdvisor.SelectedItem.ToString())));
                ProjectAdvisor.Parameters.AddWithValue("@ProjectId", projects[0].ID);
                ProjectAdvisor.Parameters.AddWithValue("@AdvisorRole", getAdvisorRole("Main Advisor"));
                ProjectAdvisor.Parameters.AddWithValue("@Date", DateTime.Now.ToString("yyyy-MM-dd"));
                ProjectAdvisor.ExecuteNonQuery();

                SqlCommand ProjectAdvisor1 = new SqlCommand("insert into [dbo].[ProjectAdvisor](AdvisorId , ProjectId , AdvisorRole , AssignmentDate) values (@AdvisorId , @ProjectId , @AdvisorRole , @Date)\r\n" , con);
                ProjectAdvisor1.Parameters.AddWithValue("@AdvisorId", getID(getFirstName(CoAdvisor.SelectedItem.ToString()), getLastName(CoAdvisor.SelectedItem.ToString())));
                ProjectAdvisor1.Parameters.AddWithValue("@ProjectId", projects[0].ID);
                ProjectAdvisor1.Parameters.AddWithValue("@AdvisorRole", getAdvisorRole("Co-Advisror"));
                ProjectAdvisor1.Parameters.AddWithValue("@Date", DateTime.Now.ToString("yyyy-MM-dd"));
                ProjectAdvisor1.ExecuteNonQuery();

                SqlCommand ProjectAdvisor2 = new SqlCommand("insert into [dbo].[ProjectAdvisor](AdvisorId , ProjectId , AdvisorRole , AssignmentDate) values (@AdvisorId , @ProjectId , @AdvisorRole , @Date)\r\n", con);
                ProjectAdvisor2.Parameters.AddWithValue("@AdvisorId", getID(getFirstName(IndustryAdvisor.SelectedItem.ToString()), getLastName(IndustryAdvisor.SelectedItem.ToString())));
                ProjectAdvisor2.Parameters.AddWithValue("@ProjectId", projects[0].ID);
                ProjectAdvisor2.Parameters.AddWithValue("@AdvisorRole", getAdvisorRole("Industry Advisor"));
                ProjectAdvisor2.Parameters.AddWithValue("@Date", DateTime.Now.ToString("yyyy-MM-dd"));
                ProjectAdvisor2.ExecuteNonQuery();
                MessageBox.Show("Group has been made...");
                refresh();
                refreshAllStudents();
                SelectedProjects.DataSource = null;
                SelectedStudents .DataSource = null;
                students.Clear();
                projects.Clear();
                refreshAllProjects();
                CoAdvisor.SelectedIndex = 0;
                MainAdvisor.SelectedIndex = 0;
                IndustryAdvisor.SelectedIndex = 0;
            }
        }

        private void txtStudentSearch_TextChanged(object sender, EventArgs e)
        {

        }



        private void SelectedStudents_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            selectedStudentID = int.Parse(SelectedStudents.Rows[e.RowIndex].Cells[0].Value.ToString());
            registrationNumber = SelectedStudents.Rows[e.RowIndex].Cells[1].Value.ToString();
            Firstname = SelectedStudents.Rows[e.RowIndex].Cells[2].Value.ToString();
            Lastname = SelectedStudents.Rows[e.RowIndex].Cells[3].Value.ToString();
            Contact = SelectedStudents.Rows[e.RowIndex].Cells[4].Value.ToString();
            Email = SelectedStudents.Rows[e.RowIndex].Cells[5].Value.ToString();
        }

        private void AllStudents_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            ID = int.Parse(AllStudents.Rows[e.RowIndex].Cells[0].Value.ToString());
            registrationNumber = AllStudents.Rows[e.RowIndex].Cells[1].Value.ToString();
            Firstname = AllStudents.Rows[e.RowIndex].Cells[2].Value.ToString();
            Lastname = AllStudents.Rows[e.RowIndex].Cells[3].Value.ToString();
            Contact = AllStudents.Rows[e.RowIndex].Cells[4].Value.ToString();
            Email = AllStudents.Rows[e.RowIndex].Cells[5].Value.ToString();
            student = new Student(ID, registrationNumber, Firstname, Lastname, Contact, Email);
            if (students.Count < 3)
            {
                if (!chechStudentisPresent(student))
                {
                    students.Add(student);
                    refresh();
                }
                else
                {
                    MessageBox.Show("Student is already present in the group...");
                }
            }
            else
            {
                MessageBox.Show("Group cannot have more than 3 students...");
            }
        }

        private void SelectedStudents_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            selectedStudentID = int.Parse(SelectedStudents.Rows[e.RowIndex].Cells[0].Value.ToString());
            registrationNumber = SelectedStudents.Rows[e.RowIndex].Cells[1].Value.ToString();
            Firstname = SelectedStudents.Rows[e.RowIndex].Cells[2].Value.ToString();
            Lastname = SelectedStudents.Rows[e.RowIndex].Cells[3].Value.ToString();
            Contact = SelectedStudents.Rows[e.RowIndex].Cells[4].Value.ToString();
            Email = SelectedStudents.Rows[e.RowIndex].Cells[5].Value.ToString();
        }

        private void AllProjects_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            ID = int.Parse(AllProjects.Rows[e.RowIndex].Cells[0].Value.ToString());
            Title = AllProjects.Rows[e.RowIndex].Cells[1].Value.ToString();
            Description = AllProjects.Rows[e.RowIndex].Cells[2].Value.ToString();
            project = new BL.Project(Title, Description, ID);
            if (projects.Count < 1)
            {
                projects.Add(project);
            }
            refreshSelectedProjects();
        }

        private void AllProjects_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            ID = int.Parse(AllProjects.Rows[e.RowIndex].Cells[0].Value.ToString());
            Title = AllProjects.Rows[e.RowIndex].Cells[1].Value.ToString();
            Description = AllProjects.Rows[e.RowIndex].Cells[2].Value.ToString();
            project = new BL.Project(Title, Description, ID);
            if (projects.Count < 1)
            {
                projects.Add(project);
            }
            refreshSelectedProjects();
        }

        private void AllStudents_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            ID = int.Parse(AllStudents.Rows[e.RowIndex].Cells[0].Value.ToString());
            registrationNumber = AllStudents.Rows[e.RowIndex].Cells[1].Value.ToString();
            Firstname = AllStudents.Rows[e.RowIndex].Cells[2].Value.ToString();
            Lastname = AllStudents.Rows[e.RowIndex].Cells[3].Value.ToString();
            Contact = AllStudents.Rows[e.RowIndex].Cells[4].Value.ToString();
            Email = AllStudents.Rows[e.RowIndex].Cells[5].Value.ToString();
            student = new Student(ID, registrationNumber, Firstname, Lastname, Contact, Email);
            if (students.Count <3)
            {
                if (!chechStudentisPresent(student))
                {
                    students.Add(student);
                    refresh();
                }
                else
                {
                    MessageBox.Show("Student is already present in the group...");
                }
            }
            else
            {
                MessageBox.Show("Group cannot have more than 3 students...");
            }
        }

        private void SelectedProjects_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowIndex = e.RowIndex;
            DataGridViewRow row = AllStudents.Rows[rowIndex];
            ID = int.Parse(SelectedStudents.Rows[e.RowIndex].Cells[0].Value.ToString());
            Title = SelectedStudents.Rows[e.RowIndex].Cells[1].Value.ToString();
            Description = SelectedStudents.Rows[e.RowIndex].Cells[2].Value.ToString();
        }

        private bool chechStudentisPresent(Student s)
        { 
            for (int i = 0; i < students.Count; i++)
            {
                if (s.RegistrationNo == students[i].RegistrationNo && s.Firstname == students[i].Firstname)
                {
                    return true;
                }
            }
            return false;
        }


    }
}
