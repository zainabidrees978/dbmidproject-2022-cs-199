﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mid_Project_DB.Project
{
    public partial class View : Form
    {
        private static int ID;
        public View(string Title, string Description, int IDs)
        {
            InitializeComponent();
            txtDescription.Text = Description;
            txtTitle.Text = Title;
            ID = IDs;
        }

        private void View_Load(object sender, EventArgs e)
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            this.CenterToScreen();
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
