﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mid_Project_DB.Project
{
    public partial class ViewProject : Form
    {
        private string Title, Description;
        private int ID = -1;
        public ViewProject()
        {
            InitializeComponent();
        }

        private void ViewProject_Load(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select Id , Title , Description from Project");
            cmd.Connection = con;
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            AllProjects.DataSource = dataTable;
            AllProjects.RowHeadersVisible = true;
            AllProjects.RowHeadersWidth = 30;
        }

        private void Add_Advisor_Click(object sender, EventArgs e)
        {
            Form form = new AddProject();
            form.ShowDialog();
        }

        private void Edit_Advisor_Click(object sender, EventArgs e)
        {
            if (ID != -1)
            {
                Form form = new EditProject(Title, Description, ID);
                form.ShowDialog();
            }

            else
            {
                MessageBox.Show("Please select an entry to edit...");
            }
        }

        private void guna2Button4_Click(object sender, EventArgs e)
        {
            refresh();
        }

        private void refresh()
        {
            AllProjects.DataSource = null;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select Id , Title , Description from Project");
            cmd.Connection = con;
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            AllProjects.DataSource = dataTable;
        }


        private void Delete_Advisor_Click(object sender, EventArgs e)
        {
            if (ID != -1)
            {

                try
                {
                    if (MessageBox.Show("Are you sure?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        var con = Configuration.getInstance().getConnection();
                        SqlCommand cmd = new SqlCommand("delete Project where Id = @ID");
                        cmd.Parameters.AddWithValue("@ID", ID);
                        cmd.Connection = con;
                        cmd.ExecuteNonQuery();
                        MessageBox.Show("Record Has Been Deleted Successfully...");
                        refresh();
                    }

                }
                catch (Exception ex)
                {

                    MessageBox.Show("Project is already selected by a group so it cannot be deleted...");
                }

            }
            else
            {
                MessageBox.Show("Please Select A Row To Deleteo...");
            }
        }



        private void guna2TextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void guna2Button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtID.Text))
            {
                try
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("select Id , Title , Description from Project where Id = " + txtID.Text + "");
                    cmd.Connection = con;
                    SqlDataReader sqlData = cmd.ExecuteReader();
                    DataTable dataTable = new DataTable();
                    dataTable.Load(sqlData);
                    AllProjects.DataSource = dataTable;

                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void tableLayoutPanel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void tableLayoutPanel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void AllProjects_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {


        }

        private void AllProjects_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            Title = AllProjects.Rows[e.RowIndex].Cells[1].Value.ToString();
            Description = AllProjects.Rows[e.RowIndex].Cells[2].Value.ToString();
            ID = int.Parse(AllProjects.Rows[e.RowIndex].Cells[0].Value.ToString());
            if (ID != -1)
            {
                Form form = new View(Title, Description, ID);
                form.ShowDialog();
            }

        }

        private void AllProjects_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            Title = AllProjects.Rows[e.RowIndex].Cells[1].Value.ToString();
            Description = AllProjects.Rows[e.RowIndex].Cells[2].Value.ToString();
            ID = int.Parse(AllProjects.Rows[e.RowIndex].Cells[0].Value.ToString());

        }

        private void AllProjects_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
