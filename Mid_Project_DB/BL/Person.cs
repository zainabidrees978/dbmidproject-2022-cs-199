﻿using System;
using System.Collections.Generic;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mid_Project_DB.BL
{
    internal class Person
    {
        private string Role;
        private string FirstName;
        private string LastName;

        public Person(string role, string firstName, string lastName)
        {
            Role1 = role;
            FirstName1 = firstName;
            LastName1 = lastName;
        }

        public string FirstName1 { get => FirstName; set => FirstName = value; }
        public string LastName1 { get => LastName; set => LastName = value; }
        public string Role1 { get => Role; set => Role = value; }
    }
}
