﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mid_Project_DB.BL
{
    internal class Group
    {
        private int ID;
        private string createdOn;
        private int SID;
        private string RegistrationNumber;

        public Group(int ID , string createdOn , int SID , string RegNo)
        {
            this.ID1 = ID;
            this.CreatedOn = createdOn;
            this.SID1 = SID;
            this.RegistrationNumber1 = RegNo;
        }

        public int ID1 { get => ID; set => ID = value; }
        public string CreatedOn { get => createdOn; set => createdOn = value; }
        public int SID1 { get => SID; set => SID = value; }
        public string RegistrationNumber1 { get => RegistrationNumber; set => RegistrationNumber = value; }
    }
}
