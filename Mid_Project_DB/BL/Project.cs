﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mid_Project_DB.BL
{
    internal class Project
    {
        private int Id;
        private string title;
        private string description;

        public int ID { get => Id; set => Id = value; }

        public string Title { get => title; set => title = value; }
        public string Description { get => description; set => description = value; }

        public Project(string title, string description, int iD)
        {
            this.Title = title;
            this.Description = description;
            this.ID = iD;
        }   
    }
}
