﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mid_Project_DB.BL
{
    internal class Evaluation
    {
        private int ID;
        private string Name;
        private float totalMarks;
        private float totalWeightage;


        public Evaluation(int ID , string Name ,float totalMark , float totalWeightage) 
        {
            this.ID1 = ID;
            this.Name1 = Name;
            this.TotalMarks = totalMark;
            this.TotalWeightage = totalWeightage;
        }

        public int ID1 { get => ID; set => ID = value; }
        public string Name1 { get => Name; set => Name = value; }
        public float TotalMarks { get => totalMarks; set => totalMarks = value; }
        public float TotalWeightage { get => totalWeightage; set => totalWeightage = value; }
    }
}
