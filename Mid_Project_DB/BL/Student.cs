﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mid_Project_DB.BL
{
     public class Student
    {
        private int Id;
        private string registrationNo1;
        private string firstname1;
        private string lastname1;
        private string contact1;
        private string email1;

        public int ID { get => Id; set => Id = value; }
        public string RegistrationNo { get => registrationNo1; set => registrationNo1 = value; }
        public string Firstname { get => firstname1; set => firstname1 = value; }
        public string Lastname { get => lastname1; set => lastname1 = value; }
        public string Contact { get => contact1; set => contact1 = value; }
        public string Email { get => email1; set => email1 = value; }

        public Student(int ID,string registrationNo , string FirstName , string Lastname , string Contact , string Email)
        {
            this.ID = ID;
            this.RegistrationNo = registrationNo;
            this.Contact = Contact;
            this.Email = Email;
            this.Firstname = FirstName;
            this.Lastname= Lastname;
        }

    }
}
