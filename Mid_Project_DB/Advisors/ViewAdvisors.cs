﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mid_Project_DB.Advisors
{
    public partial class ViewAdvisors : Form
    {
        int ID = -1;
        string FirstName;
        string LastName, Contact, Email , DOB;
        int salary, GEnder, Designation;

        public ViewAdvisors()
        {
            InitializeComponent();
        }

        private void ViewAdvisors_Load(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select P1.Id , P1.FirstName , P1.LastName ,L.Value as Gender, P1.Contact , P1.Email , P1.DateOfBirth , P1.Designation , P1.Salary \r\nfrom Lookup as L\r\njoin (select A.Id , P.FirstName , P.LastName , P.Contact , P.Email , P.DateOfBirth , L.Value as Designation , P.Gender\r\nfrom Advisor as A\r\njoin Person  as P\r\non P.Id = A.Id\r\njoin Lookup as L\r\non L.Id = A.Designation) as P1\r\non P1.Gender = L.Id");
            cmd.Connection = con;
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            dataGridView1.DataSource = dataTable;
            dataGridView1.RowHeadersVisible = true;
            dataGridView1.RowHeadersWidth = 30;
        }

        private void ViewAdvisors_Load_1(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select P1.Id , P1.FirstName , P1.LastName ,L.Value as Gender, P1.Contact , P1.Email , P1.DateOfBirth , P1.Designation , P1.Salary \r\nfrom Lookup as L\r\njoin (select A.Id , P.FirstName , P.LastName , P.Contact , P.Email , P.DateOfBirth , L.Value as Designation , P.Gender , A.Salary\r\nfrom Advisor as A\r\njoin Person  as P\r\non P.Id = A.Id\r\njoin Lookup as L\r\non L.Id = A.Designation) as P1\r\non P1.Gender = L.Id");
            cmd.Connection = con;
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            dataGridView1.DataSource = dataTable;
            dataGridView1.RowHeadersVisible = true;
            dataGridView1.RowHeadersWidth = 30;
        }

        private void guna2DataGridView5_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void guna2Button4_Click(object sender, EventArgs e)
        {
            Form form = new AddStaff();
            form.Show();
        }

        private void Delete_Advisor_Click(object sender, EventArgs e)
        {
            if (ID != -1)
            {
                try
                {
                    if (MessageBox.Show("Are you sure?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        var con = Configuration.getInstance().getConnection();
                        SqlCommand cmd = new SqlCommand("delete Advisor where Id = @ID; delete Person where Id = @ID;");
                        cmd.Parameters.AddWithValue("@ID", ID);
                        cmd.Connection = con;
                        cmd.ExecuteNonQuery();
                        MessageBox.Show("Record Has Been Deleted Successfully...");
                        refresh();
                        ID = -1;
                    }

                }
                catch (Exception ex)
                {

                    MessageBox.Show("You cannot delete this Advisor...");
                }

            }
            else
            {
                MessageBox.Show("Please Select A Row To Deleteo...");
            }
        }

        private void guna2Button4_Click_1(object sender, EventArgs e)
        {
            refresh();
        }

        private void refresh()
        {
            dataGridView1.DataSource = null;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select P1.Id , P1.FirstName , P1.LastName ,L.Value as Gender, P1.Contact , P1.Email , P1.DateOfBirth , P1.Designation , P1.Salary \r\nfrom Lookup as L\r\njoin (select A.Id , P.FirstName , P.LastName , P.Contact , P.Email , P.DateOfBirth , L.Value as Designation , P.Gender , A.Salary \r\nfrom Advisor as A\r\njoin Person  as P\r\non P.Id = A.Id\r\njoin Lookup as L\r\non L.Id = A.Designation) as P1\r\non P1.Gender = L.Id");
            cmd.Connection = con;
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            dataGridView1.DataSource = dataTable;
        }

        private void tableLayoutPanel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void guna2Button5_Click(object sender, EventArgs e)
        {

        }

        private void guna2Button6_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtSearch.Text))
            {
                try
                {
                    dataGridView1.DataSource = null;
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("select P1.Id , P1.FirstName , P1.LastName ,L.Value as Gender, P1.Contact , P1.Email , P1.DateOfBirth , P1.Designation , P1.Salary \r\nfrom Lookup as L\r\njoin (select A.Id , P.FirstName , P.LastName , P.Contact , P.Email , P.DateOfBirth , L.Value as Designation , P.Gender , A.Salary \r\nfrom Advisor as A\r\njoin Person  as P\r\non P.Id = A.Id\r\njoin Lookup as L\r\non L.Id = A.Designation) as P1\r\non P1.Gender = L.Id where P1.Id = " + txtSearch.Text + "");
                    cmd.Connection = con;
                    SqlDataReader sqlData = cmd.ExecuteReader();
                    DataTable dataTable = new DataTable();
                    dataTable.Load(sqlData);
                    dataGridView1.DataSource = dataTable;
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }

            }
        }

        private void dataGridView1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            ID = int.Parse(dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString());
            FirstName = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            LastName = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();
            DOB = dataGridView1.Rows[e.RowIndex].Cells[6].Value.ToString();
            if (dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString() == "Male")
            {
                GEnder = 1;
            }
            else
            {
                GEnder = 2;
            }
            Contact = dataGridView1.Rows[e.RowIndex].Cells[4].Value.ToString();
            Email = dataGridView1.Rows[e.RowIndex].Cells[5].Value.ToString();
            if (dataGridView1.Rows[e.RowIndex].Cells[7].Value.ToString() == "Professor")
            {
                Designation = 1;
            }
            else if (dataGridView1.Rows[e.RowIndex].Cells[7].Value.ToString() == "Associate Professor")
            {
                Designation = 2;
            }
            else if (dataGridView1.Rows[e.RowIndex].Cells[7].Value.ToString() == "Assisstant Professor")
            {
                Designation = 3;
            }
            else if (dataGridView1.Rows[e.RowIndex].Cells[7].Value.ToString() == "Lecturer")
            {
                Designation = 4;
            }
            else
            {
                Designation = 5;
            }
            salary = int.Parse(dataGridView1.Rows[e.RowIndex].Cells[8].Value.ToString());
        }

        private void dataGridView1_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            ID = int.Parse(dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString());
            FirstName = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            LastName = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();
            DOB = dataGridView1.Rows[e.RowIndex].Cells[6].Value.ToString();
            if (dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString() == "Male")
            {
                GEnder = 1;
            }
            else
            {
                GEnder = 2;
            }
            Contact = dataGridView1.Rows[e.RowIndex].Cells[4].Value.ToString();
            Email = dataGridView1.Rows[e.RowIndex].Cells[5].Value.ToString();
            if (dataGridView1.Rows[e.RowIndex].Cells[7].Value.ToString() == "Professor")
            {
                Designation = 1;
            }
            else if (dataGridView1.Rows[e.RowIndex].Cells[7].Value.ToString() == "Associate Professor")
            {
                Designation = 2;
            }
            else if (dataGridView1.Rows[e.RowIndex].Cells[7].Value.ToString() == "Assisstant Professor")
            {
                Designation = 3;
            }
            else if (dataGridView1.Rows[e.RowIndex].Cells[7].Value.ToString() == "Lecturer")
            {
                Designation = 4;
            }
            else
            {
                Designation = 5;
            }
            salary = int.Parse(dataGridView1.Rows[e.RowIndex].Cells[8].Value.ToString());
        }

        private void dataGridView1_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtSearch_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13)
            {
                if (!string.IsNullOrEmpty(txtSearch.Text))
                {
                    try
                    {
                        dataGridView1.DataSource = null;
                        var con = Configuration.getInstance().getConnection();
                        SqlCommand cmd = new SqlCommand("select P1.Id , P1.FirstName , P1.LastName ,L.Value as Gender, P1.Contact , P1.Email , P1.DateOfBirth , P1.Designation , P1.Salary \r\nfrom Lookup as L\r\njoin (select A.Id , P.FirstName , P.LastName , P.Contact , P.Email , P.DateOfBirth , L.Value as Designation , P.Gender , A.Salary \r\nfrom Advisor as A\r\njoin Person  as P\r\non P.Id = A.Id\r\njoin Lookup as L\r\non L.Id = A.Designation) as P1\r\non P1.Gender = L.Id where P1.Id = " + txtSearch.Text + "");
                        cmd.Connection = con;
                        SqlDataReader sqlData = cmd.ExecuteReader();
                        DataTable dataTable = new DataTable();
                        dataTable.Load(sqlData);
                        dataGridView1.DataSource = dataTable;
                    }
                    catch (Exception ex)
                    {

                        MessageBox.Show(ex.Message);
                    }

                }
            }
        }

        private void dataGridView1_RowHeaderMouseDoubleClick_1(object sender, DataGridViewCellMouseEventArgs e)
        {
            
        }


        private void Edit_Advisor_Click(object sender, EventArgs e)
        {
            if(ID != -1)
            {
                Form form = new EditAdvisor(FirstName, LastName, Contact, Email, salary, GEnder, Designation, DOB, ID);
                form.Show();
            }
            else
            {
                MessageBox.Show("Please select an entry to edit...");
            }
        }

       

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
