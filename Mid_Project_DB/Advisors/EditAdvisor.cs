﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ListView;

namespace Mid_Project_DB.Advisors
{
    public partial class EditAdvisor : Form
    {
        private int IDs;
        private int genderI , DesignationI;
        public EditAdvisor(string FirstName, string LastName, string Contact, string email, int Salary, int Gender, int Designations, string DOB , int ID)
        {
            InitializeComponent();
            txtContact.Text = Contact;
            txtEmail.Text = email;
            txtFirstName.Text = FirstName;
            txtLastName.Text = LastName;
            genderI = Gender;
            DesignationI = Designations;
            txtSalary.Text = Salary.ToString();
            DOBpicker.Value = DateTime.Parse(DOB);
            IDs = ID;
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private bool checkID(string s)
        {
            for (int i = 0; i < s.Length; i++)
            {
                if (!Char.IsDigit(s[i]))
                {
                    return false;
                }
            }
            return true;
        }

        private void AddAdvisor_Click(object sender, EventArgs e)
        {
            Salary.Visible = false;
            if (string.IsNullOrEmpty(txtSalary.Text) || string.IsNullOrEmpty(txtContact.Text) || string.IsNullOrEmpty(txtEmail.Text) || string.IsNullOrEmpty(txtFirstName.Text) || string.IsNullOrEmpty(txtLastName.Text))
            {
                MessageBox.Show("Please Fill All Queries...");
            }
            else
            {

                if (!checkID(txtSalary.Text))
                {
                    Salary.Text = "*Salary can only consist of digits.";
                    Salary.Visible = true;
                }
                else
                {
                    try
                    {
                        var con = Configuration.getInstance().getConnection();
                        SqlCommand cmd = new SqlCommand("update [dbo].[Person] " +
                        " set FirstName = @FirstName, LastName = @LastName, Contact = @Contact, Email = @Email, DateOfBirth = @DOB, Gender = @Gender "+
                        " where Id = @ID; "+
                        " update [dbo].[Advisor]" +
                        " set Designation = @Designation, Salary = @Salary"+
                        " where Id = @ID ", con);
                        cmd.Parameters.AddWithValue("@Salary", int.Parse(txtSalary.Text));
                        cmd.Parameters.AddWithValue("@Designation", Designation.SelectedIndex + 6);
                        cmd.Parameters.AddWithValue("@FirstName", txtFirstName.Text);
                        cmd.Parameters.AddWithValue("@LastName", txtLastName.Text);
                        cmd.Parameters.AddWithValue("@Contact", txtContact.Text);
                        cmd.Parameters.AddWithValue("@Email", txtEmail.Text);
                        cmd.Parameters.AddWithValue("@Gender", Genders.SelectedIndex+1);
                        cmd.Parameters.AddWithValue("@ID", IDs);
                        DOBpicker.Format = DateTimePickerFormat.Custom;
                        DOBpicker.CustomFormat = "yyyy-MM-dd";
                        cmd.Parameters.AddWithValue("@DOB", DOBpicker.Text);
                        cmd.ExecuteNonQuery();
                        MessageBox.Show("Successfully Updated");
                        this.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
            }
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            this.Close(); 
        }

        private void EditAdvisor_Load(object sender, EventArgs e)
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            this.CenterToScreen();

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd1 = new SqlCommand("select Value from Lookup where Category = 'GENDER'");
            cmd1.Connection = con;
            SqlDataReader sqlData1 = cmd1.ExecuteReader();
            DataTable dataTable1 = new DataTable();
            dataTable1.Load(sqlData1);
            List<string> list1 = new List<string>();
            for (int i = 0; i < dataTable1.Rows.Count; i++)
            {
                string s = dataTable1.Rows[i]["Value"].ToString();
                list1.Add(s);
            }
            Genders.DataSource = list1;
            Genders.SelectedIndex = genderI - 1;

            SqlCommand cmd = new SqlCommand("select Value from Lookup where Category = 'DESIGNATION'");
            cmd.Connection = con;
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            List<string> list = new List<string>();
            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                string s = dataTable.Rows[i]["Value"].ToString();
                list.Add(s);
            }
            Designation.DataSource = list;
            Designation.SelectedIndex = DesignationI -1;
        }
    }
}
