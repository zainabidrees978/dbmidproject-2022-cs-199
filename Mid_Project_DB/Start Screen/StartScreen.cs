﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mid_Project_DB.Start_Screen
{
    public partial class StartScreen : Form
    {
        public StartScreen()
        {
            InitializeComponent();
        }

        private void StartScreen_Load(object sender, EventArgs e)
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            this.CenterToScreen();
        }

        private void guna2Button1_Click(object sender, EventArgs e)
        {
            ProgressBarTimer.Enabled = true;
            ProgressBarTimer.Start();
        }

        private void ProgressBarTimer_Tick(object sender, EventArgs e)
        {
            //ProgressBarTimer.Enabled = true;
            ProgressBar1.Increment(1);
            if (ProgressBar1.Value == 100)
            {
                ProgressBarTimer.Stop();
                Form form = new Main();
                this.Visible = false;
                form.ShowDialog();
                this.Hide();
                this.Close();
            }
        }

        private void Close_Click(object sender, EventArgs e)
        {
            Application.Exit();
            this.Close();
        }

        private void guna2Panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
