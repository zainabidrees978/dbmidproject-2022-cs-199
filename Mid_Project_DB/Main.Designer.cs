﻿namespace Mid_Project_DB
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.SidebadAnimation = new System.Windows.Forms.Timer(this.components);
            this.StudentsTimer = new System.Windows.Forms.Timer(this.components);
            this.AdvisorTimer = new System.Windows.Forms.Timer(this.components);
            this.guna2CustomGradientPanel1 = new Guna.UI2.WinForms.Guna2CustomGradientPanel();
            this.Minimize = new Guna.UI2.WinForms.Guna2ImageButton();
            this.Maximize = new Guna.UI2.WinForms.Guna2ImageButton();
            this.Close = new Guna.UI2.WinForms.Guna2ImageButton();
            this.Menu = new Guna.UI2.WinForms.Guna2ImageButton();
            this.GroupTimer = new System.Windows.Forms.Timer(this.components);
            this.guna2DragControl1 = new Guna.UI2.WinForms.Guna2DragControl(this.components);
            this.EvaluateGroupTimer = new System.Windows.Forms.Timer(this.components);
            this.guna2Elipse1 = new Guna.UI2.WinForms.Guna2Elipse(this.components);
            this.Exit = new Guna.UI2.WinForms.Guna2Button();
            this.panelChildForm = new Guna.UI2.WinForms.Guna2ShadowPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.StudentPanel = new System.Windows.Forms.Panel();
            this.Students = new Guna.UI2.WinForms.Guna2Button();
            this.AddStudent = new Guna.UI2.WinForms.Guna2Button();
            this.AdvisorsPanel = new System.Windows.Forms.Panel();
            this.guna2Button1 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button2 = new Guna.UI2.WinForms.Guna2Button();
            this.AddProject = new Guna.UI2.WinForms.Guna2Button();
            this.GroupsPanel = new Guna.UI2.WinForms.Guna2Panel();
            this.guna2Button4 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button3 = new Guna.UI2.WinForms.Guna2Button();
            this.ViewGroup = new Guna.UI2.WinForms.Guna2Button();
            this.Groups = new Guna.UI2.WinForms.Guna2Button();
            this.Evaluations = new Guna.UI2.WinForms.Guna2Button();
            this.EvaluationPanel = new Guna.UI2.WinForms.Guna2Panel();
            this.guna2Button7 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button6 = new Guna.UI2.WinForms.Guna2Button();
            this.EvManage = new Guna.UI2.WinForms.Guna2Button();
            this.GenerateReport = new Guna.UI2.WinForms.Guna2Button();
            this.guna2CustomGradientPanel1.SuspendLayout();
            this.panelChildForm.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.StudentPanel.SuspendLayout();
            this.AdvisorsPanel.SuspendLayout();
            this.GroupsPanel.SuspendLayout();
            this.EvaluationPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // SidebadAnimation
            // 
            this.SidebadAnimation.Interval = 2;
            this.SidebadAnimation.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // StudentsTimer
            // 
            this.StudentsTimer.Interval = 5;
            this.StudentsTimer.Tick += new System.EventHandler(this.StudentsTimer_Tick);
            // 
            // AdvisorTimer
            // 
            this.AdvisorTimer.Interval = 5;
            this.AdvisorTimer.Tick += new System.EventHandler(this.AdvisorTimer_Tick);
            // 
            // guna2CustomGradientPanel1
            // 
            resources.ApplyResources(this.guna2CustomGradientPanel1, "guna2CustomGradientPanel1");
            this.guna2CustomGradientPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(24)))), ((int)(((byte)(96)))));
            this.guna2CustomGradientPanel1.BorderRadius = 50;
            this.guna2CustomGradientPanel1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Dash;
            this.guna2CustomGradientPanel1.Controls.Add(this.Minimize);
            this.guna2CustomGradientPanel1.Controls.Add(this.Maximize);
            this.guna2CustomGradientPanel1.Controls.Add(this.Close);
            this.guna2CustomGradientPanel1.Controls.Add(this.Menu);
            this.guna2CustomGradientPanel1.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(24)))), ((int)(((byte)(96)))));
            this.guna2CustomGradientPanel1.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(24)))), ((int)(((byte)(96)))));
            this.guna2CustomGradientPanel1.FillColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(24)))), ((int)(((byte)(96)))));
            this.guna2CustomGradientPanel1.FillColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(24)))), ((int)(((byte)(96)))));
            this.guna2CustomGradientPanel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(24)))), ((int)(((byte)(96)))));
            this.guna2CustomGradientPanel1.Name = "guna2CustomGradientPanel1";
            this.guna2CustomGradientPanel1.DragDrop += new System.Windows.Forms.DragEventHandler(this.guna2CustomGradientPanel1_DragDrop);
            this.guna2CustomGradientPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.guna2CustomGradientPanel1_Paint);
            // 
            // Minimize
            // 
            this.Minimize.CheckedState.ImageSize = new System.Drawing.Size(64, 64);
            resources.ApplyResources(this.Minimize, "Minimize");
            this.Minimize.HoverState.ImageSize = new System.Drawing.Size(30, 30);
            this.Minimize.Image = global::Mid_Project_DB.Properties.Resources.minimize_window_500px;
            this.Minimize.ImageOffset = new System.Drawing.Point(0, 0);
            this.Minimize.ImageRotate = 0F;
            this.Minimize.ImageSize = new System.Drawing.Size(30, 30);
            this.Minimize.Name = "Minimize";
            this.Minimize.PressedState.ImageSize = new System.Drawing.Size(64, 64);
            this.Minimize.Click += new System.EventHandler(this.guna2ImageButton3_Click);
            // 
            // Maximize
            // 
            this.Maximize.CheckedState.ImageSize = new System.Drawing.Size(64, 64);
            resources.ApplyResources(this.Maximize, "Maximize");
            this.Maximize.HoverState.ImageSize = new System.Drawing.Size(30, 30);
            this.Maximize.Image = global::Mid_Project_DB.Properties.Resources.maximize_window_500px;
            this.Maximize.ImageOffset = new System.Drawing.Point(0, 0);
            this.Maximize.ImageRotate = 0F;
            this.Maximize.ImageSize = new System.Drawing.Size(30, 30);
            this.Maximize.Name = "Maximize";
            this.Maximize.PressedState.Image = global::Mid_Project_DB.Properties.Resources.maximize_button_144px;
            this.Maximize.PressedState.ImageSize = new System.Drawing.Size(30, 30);
            this.Maximize.Click += new System.EventHandler(this.guna2ImageButton2_Click);
            // 
            // Close
            // 
            this.Close.BackColor = System.Drawing.Color.Transparent;
            this.Close.CheckedState.ImageSize = new System.Drawing.Size(64, 64);
            resources.ApplyResources(this.Close, "Close");
            this.Close.HoverState.ImageSize = new System.Drawing.Size(30, 30);
            this.Close.Image = global::Mid_Project_DB.Properties.Resources.close_window_500px;
            this.Close.ImageOffset = new System.Drawing.Point(0, 0);
            this.Close.ImageRotate = 0F;
            this.Close.ImageSize = new System.Drawing.Size(30, 30);
            this.Close.Name = "Close";
            this.Close.PressedState.ImageSize = new System.Drawing.Size(30, 30);
            this.Close.UseTransparentBackground = true;
            this.Close.Click += new System.EventHandler(this.guna2ImageButton1_Click);
            // 
            // Menu
            // 
            this.Menu.BackColor = System.Drawing.Color.Transparent;
            this.Menu.CheckedState.ImageSize = new System.Drawing.Size(64, 64);
            this.Menu.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Menu.HoverState.ImageSize = new System.Drawing.Size(40, 40);
            this.Menu.Image = global::Mid_Project_DB.Properties.Resources.xbox_menu_500px;
            this.Menu.ImageOffset = new System.Drawing.Point(0, 0);
            this.Menu.ImageRotate = 0F;
            this.Menu.ImageSize = new System.Drawing.Size(40, 40);
            resources.ApplyResources(this.Menu, "Menu");
            this.Menu.Name = "Menu";
            this.Menu.PressedState.ImageSize = new System.Drawing.Size(40, 40);
            this.Menu.UseTransparentBackground = true;
            this.Menu.Click += new System.EventHandler(this.Menu_Click);
            // 
            // GroupTimer
            // 
            this.GroupTimer.Interval = 5;
            this.GroupTimer.Tick += new System.EventHandler(this.GroupTimer_Tick);
            // 
            // guna2DragControl1
            // 
            this.guna2DragControl1.DockIndicatorTransparencyValue = 0.6D;
            this.guna2DragControl1.TargetControl = this.guna2CustomGradientPanel1;
            this.guna2DragControl1.UseTransparentDrag = true;
            // 
            // EvaluateGroupTimer
            // 
            this.EvaluateGroupTimer.Interval = 5;
            this.EvaluateGroupTimer.Tick += new System.EventHandler(this.EvaluateGroupTimer_Tick);
            // 
            // guna2Elipse1
            // 
            this.guna2Elipse1.BorderRadius = 15;
            this.guna2Elipse1.TargetControl = this;
            // 
            // Exit
            // 
            this.Exit.Animated = true;
            this.Exit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Exit.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.Exit.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.Exit.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.Exit.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.Exit.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(48)))), ((int)(((byte)(120)))));
            resources.ApplyResources(this.Exit, "Exit");
            this.Exit.ForeColor = System.Drawing.Color.White;
            this.Exit.Image = global::Mid_Project_DB.Properties.Resources.Exit;
            this.Exit.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.Exit.ImageSize = new System.Drawing.Size(30, 30);
            this.Exit.IndicateFocus = true;
            this.Exit.Name = "Exit";
            this.Exit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.Exit.Click += new System.EventHandler(this.guna2Button4_Click);
            // 
            // panelChildForm
            // 
            this.panelChildForm.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.panelChildForm, "panelChildForm");
            this.panelChildForm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelChildForm.Controls.Add(this.flowLayoutPanel1);
            this.panelChildForm.FillColor = System.Drawing.Color.Transparent;
            this.panelChildForm.ForeColor = System.Drawing.Color.Transparent;
            this.panelChildForm.Name = "panelChildForm";
            this.panelChildForm.ShadowColor = System.Drawing.Color.Transparent;
            this.panelChildForm.ShadowDepth = 5;
            this.panelChildForm.ShadowShift = 2;
            this.panelChildForm.Paint += new System.Windows.Forms.PaintEventHandler(this.panelChildForm_Paint_2);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(24)))), ((int)(((byte)(96)))));
            this.flowLayoutPanel1.Controls.Add(this.StudentPanel);
            this.flowLayoutPanel1.Controls.Add(this.AdvisorsPanel);
            this.flowLayoutPanel1.Controls.Add(this.AddProject);
            this.flowLayoutPanel1.Controls.Add(this.GroupsPanel);
            this.flowLayoutPanel1.Controls.Add(this.Evaluations);
            this.flowLayoutPanel1.Controls.Add(this.EvaluationPanel);
            this.flowLayoutPanel1.Controls.Add(this.GenerateReport);
            this.flowLayoutPanel1.Controls.Add(this.Exit);
            resources.ApplyResources(this.flowLayoutPanel1, "flowLayoutPanel1");
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.flowLayoutPanel1_Paint);
            // 
            // StudentPanel
            // 
            this.StudentPanel.Controls.Add(this.Students);
            this.StudentPanel.Controls.Add(this.AddStudent);
            resources.ApplyResources(this.StudentPanel, "StudentPanel");
            this.StudentPanel.Name = "StudentPanel";
            // 
            // Students
            // 
            this.Students.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.Students.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.Students.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.Students.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.Students.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(48)))), ((int)(((byte)(120)))));
            resources.ApplyResources(this.Students, "Students");
            this.Students.ForeColor = System.Drawing.Color.White;
            this.Students.Image = ((System.Drawing.Image)(resources.GetObject("Students.Image")));
            this.Students.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.Students.ImageSize = new System.Drawing.Size(30, 30);
            this.Students.Name = "Students";
            this.Students.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.Students.Click += new System.EventHandler(this.guna2Button1_Click);
            // 
            // AddStudent
            // 
            this.AddStudent.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.AddStudent.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.AddStudent.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.AddStudent.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.AddStudent.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(95)))), ((int)(((byte)(173)))));
            resources.ApplyResources(this.AddStudent, "AddStudent");
            this.AddStudent.ForeColor = System.Drawing.Color.White;
            this.AddStudent.Image = ((System.Drawing.Image)(resources.GetObject("AddStudent.Image")));
            this.AddStudent.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.AddStudent.Name = "AddStudent";
            this.AddStudent.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.AddStudent.Click += new System.EventHandler(this.AddStudent_Click);
            // 
            // AdvisorsPanel
            // 
            this.AdvisorsPanel.Controls.Add(this.guna2Button1);
            this.AdvisorsPanel.Controls.Add(this.guna2Button2);
            resources.ApplyResources(this.AdvisorsPanel, "AdvisorsPanel");
            this.AdvisorsPanel.Name = "AdvisorsPanel";
            // 
            // guna2Button1
            // 
            this.guna2Button1.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button1.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button1.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button1.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button1.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(95)))), ((int)(((byte)(173)))));
            resources.ApplyResources(this.guna2Button1, "guna2Button1");
            this.guna2Button1.ForeColor = System.Drawing.Color.White;
            this.guna2Button1.Image = ((System.Drawing.Image)(resources.GetObject("guna2Button1.Image")));
            this.guna2Button1.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.guna2Button1.Name = "guna2Button1";
            this.guna2Button1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.guna2Button1.Click += new System.EventHandler(this.guna2Button1_Click_1);
            // 
            // guna2Button2
            // 
            this.guna2Button2.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button2.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button2.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button2.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button2.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(48)))), ((int)(((byte)(120)))));
            resources.ApplyResources(this.guna2Button2, "guna2Button2");
            this.guna2Button2.ForeColor = System.Drawing.Color.White;
            this.guna2Button2.Image = global::Mid_Project_DB.Properties.Resources.student;
            this.guna2Button2.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.guna2Button2.ImageSize = new System.Drawing.Size(30, 30);
            this.guna2Button2.Name = "guna2Button2";
            this.guna2Button2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.guna2Button2.Click += new System.EventHandler(this.guna2Button2_Click);
            // 
            // AddProject
            // 
            this.AddProject.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.AddProject.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.AddProject.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.AddProject.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.AddProject.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.AddProject.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(48)))), ((int)(((byte)(120)))));
            resources.ApplyResources(this.AddProject, "AddProject");
            this.AddProject.ForeColor = System.Drawing.Color.White;
            this.AddProject.Image = global::Mid_Project_DB.Properties.Resources.project_144px;
            this.AddProject.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.AddProject.ImageSize = new System.Drawing.Size(30, 30);
            this.AddProject.Name = "AddProject";
            this.AddProject.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.AddProject.Click += new System.EventHandler(this.guna2Button3_Click);
            // 
            // GroupsPanel
            // 
            this.GroupsPanel.Controls.Add(this.guna2Button4);
            this.GroupsPanel.Controls.Add(this.guna2Button3);
            this.GroupsPanel.Controls.Add(this.ViewGroup);
            this.GroupsPanel.Controls.Add(this.Groups);
            resources.ApplyResources(this.GroupsPanel, "GroupsPanel");
            this.GroupsPanel.Name = "GroupsPanel";
            // 
            // guna2Button4
            // 
            this.guna2Button4.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button4.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button4.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button4.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button4.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(95)))), ((int)(((byte)(173)))));
            resources.ApplyResources(this.guna2Button4, "guna2Button4");
            this.guna2Button4.ForeColor = System.Drawing.Color.White;
            this.guna2Button4.Image = ((System.Drawing.Image)(resources.GetObject("guna2Button4.Image")));
            this.guna2Button4.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.guna2Button4.Name = "guna2Button4";
            this.guna2Button4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.guna2Button4.Click += new System.EventHandler(this.guna2Button4_Click_1);
            // 
            // guna2Button3
            // 
            this.guna2Button3.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button3.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button3.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button3.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button3.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(95)))), ((int)(((byte)(173)))));
            resources.ApplyResources(this.guna2Button3, "guna2Button3");
            this.guna2Button3.ForeColor = System.Drawing.Color.White;
            this.guna2Button3.Image = ((System.Drawing.Image)(resources.GetObject("guna2Button3.Image")));
            this.guna2Button3.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.guna2Button3.Name = "guna2Button3";
            this.guna2Button3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.guna2Button3.Click += new System.EventHandler(this.guna2Button3_Click_1);
            // 
            // ViewGroup
            // 
            this.ViewGroup.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.ViewGroup.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.ViewGroup.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.ViewGroup.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.ViewGroup.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(95)))), ((int)(((byte)(173)))));
            resources.ApplyResources(this.ViewGroup, "ViewGroup");
            this.ViewGroup.ForeColor = System.Drawing.Color.White;
            this.ViewGroup.Image = ((System.Drawing.Image)(resources.GetObject("ViewGroup.Image")));
            this.ViewGroup.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.ViewGroup.Name = "ViewGroup";
            this.ViewGroup.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.ViewGroup.Click += new System.EventHandler(this.ViewGroup_Click);
            // 
            // Groups
            // 
            this.Groups.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.Groups.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.Groups.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.Groups.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.Groups.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(48)))), ((int)(((byte)(120)))));
            resources.ApplyResources(this.Groups, "Groups");
            this.Groups.ForeColor = System.Drawing.Color.White;
            this.Groups.Image = global::Mid_Project_DB.Properties.Resources.people_90px;
            this.Groups.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.Groups.ImageSize = new System.Drawing.Size(30, 30);
            this.Groups.Name = "Groups";
            this.Groups.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.Groups.Click += new System.EventHandler(this.guna2Button5_Click);
            // 
            // Evaluations
            // 
            this.Evaluations.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Evaluations.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.Evaluations.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.Evaluations.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.Evaluations.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.Evaluations.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(48)))), ((int)(((byte)(120)))));
            resources.ApplyResources(this.Evaluations, "Evaluations");
            this.Evaluations.ForeColor = System.Drawing.Color.White;
            this.Evaluations.Image = global::Mid_Project_DB.Properties.Resources.report_card_2000px;
            this.Evaluations.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.Evaluations.ImageSize = new System.Drawing.Size(30, 30);
            this.Evaluations.Name = "Evaluations";
            this.Evaluations.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.Evaluations.TextTransform = Guna.UI2.WinForms.Enums.TextTransform.UpperCase;
            this.Evaluations.Click += new System.EventHandler(this.Evaluations_Click);
            // 
            // EvaluationPanel
            // 
            this.EvaluationPanel.Controls.Add(this.guna2Button7);
            this.EvaluationPanel.Controls.Add(this.guna2Button6);
            this.EvaluationPanel.Controls.Add(this.EvManage);
            resources.ApplyResources(this.EvaluationPanel, "EvaluationPanel");
            this.EvaluationPanel.Name = "EvaluationPanel";
            // 
            // guna2Button7
            // 
            this.guna2Button7.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.guna2Button7.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button7.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button7.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button7.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button7.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(95)))), ((int)(((byte)(173)))));
            resources.ApplyResources(this.guna2Button7, "guna2Button7");
            this.guna2Button7.ForeColor = System.Drawing.Color.White;
            this.guna2Button7.Image = ((System.Drawing.Image)(resources.GetObject("guna2Button7.Image")));
            this.guna2Button7.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.guna2Button7.Name = "guna2Button7";
            this.guna2Button7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.guna2Button7.Click += new System.EventHandler(this.guna2Button7_Click);
            // 
            // guna2Button6
            // 
            this.guna2Button6.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.guna2Button6.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button6.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button6.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button6.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button6.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(95)))), ((int)(((byte)(173)))));
            resources.ApplyResources(this.guna2Button6, "guna2Button6");
            this.guna2Button6.ForeColor = System.Drawing.Color.White;
            this.guna2Button6.Image = ((System.Drawing.Image)(resources.GetObject("guna2Button6.Image")));
            this.guna2Button6.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.guna2Button6.Name = "guna2Button6";
            this.guna2Button6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.guna2Button6.Click += new System.EventHandler(this.guna2Button6_Click);
            // 
            // EvManage
            // 
            this.EvManage.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.EvManage.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.EvManage.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.EvManage.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.EvManage.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.EvManage.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(48)))), ((int)(((byte)(120)))));
            resources.ApplyResources(this.EvManage, "EvManage");
            this.EvManage.ForeColor = System.Drawing.Color.White;
            this.EvManage.Image = global::Mid_Project_DB.Properties.Resources.audit_2000px;
            this.EvManage.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.EvManage.ImageSize = new System.Drawing.Size(30, 30);
            this.EvManage.Name = "EvManage";
            this.EvManage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.EvManage.Click += new System.EventHandler(this.EvManage_Click);
            // 
            // GenerateReport
            // 
            this.GenerateReport.Animated = true;
            this.GenerateReport.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.GenerateReport.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.GenerateReport.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.GenerateReport.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.GenerateReport.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.GenerateReport.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(48)))), ((int)(((byte)(120)))));
            resources.ApplyResources(this.GenerateReport, "GenerateReport");
            this.GenerateReport.ForeColor = System.Drawing.Color.White;
            this.GenerateReport.Image = global::Mid_Project_DB.Properties.Resources.pdf_2000px;
            this.GenerateReport.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.GenerateReport.ImageSize = new System.Drawing.Size(30, 30);
            this.GenerateReport.IndicateFocus = true;
            this.GenerateReport.Name = "GenerateReport";
            this.GenerateReport.ShadowDecoration.BorderRadius = 15;
            this.GenerateReport.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.GenerateReport.TextTransform = Guna.UI2.WinForms.Enums.TextTransform.UpperCase;
            this.GenerateReport.Click += new System.EventHandler(this.GenerateReport_Click);
            // 
            // Main
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(24)))), ((int)(((byte)(96)))));
            this.CancelButton = this.Exit;
            this.Controls.Add(this.panelChildForm);
            this.Controls.Add(this.guna2CustomGradientPanel1);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(24)))), ((int)(((byte)(96)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Main";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.guna2CustomGradientPanel1.ResumeLayout(false);
            this.panelChildForm.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.StudentPanel.ResumeLayout(false);
            this.AdvisorsPanel.ResumeLayout(false);
            this.GroupsPanel.ResumeLayout(false);
            this.EvaluationPanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Timer SidebadAnimation;
        private System.Windows.Forms.Timer StudentsTimer;
        private System.Windows.Forms.Timer AdvisorTimer;
        private Guna.UI2.WinForms.Guna2CustomGradientPanel guna2CustomGradientPanel1;
        private Guna.UI2.WinForms.Guna2ImageButton Menu;
        private System.Windows.Forms.Timer GroupTimer;
        private Guna.UI2.WinForms.Guna2Panel GroupsPanel;
        private Guna.UI2.WinForms.Guna2Button ViewGroup;
        private Guna.UI2.WinForms.Guna2Button Groups;
        private Guna.UI2.WinForms.Guna2Button AddProject;
        private System.Windows.Forms.Panel AdvisorsPanel;
        private Guna.UI2.WinForms.Guna2Button guna2Button1;
        private Guna.UI2.WinForms.Guna2Button guna2Button2;
        private Guna.UI2.WinForms.Guna2Button Exit;
        private System.Windows.Forms.Panel StudentPanel;
        private Guna.UI2.WinForms.Guna2Button Students;
        private Guna.UI2.WinForms.Guna2Button AddStudent;
        private Guna.UI2.WinForms.Guna2ShadowPanel panelChildForm;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private Guna.UI2.WinForms.Guna2Button guna2Button4;
        private Guna.UI2.WinForms.Guna2Button guna2Button3;
        private Guna.UI2.WinForms.Guna2ImageButton Close;
        private Guna.UI2.WinForms.Guna2ImageButton Maximize;
        private Guna.UI2.WinForms.Guna2ImageButton Minimize;
        private Guna.UI2.WinForms.Guna2DragControl guna2DragControl1;
        private Guna.UI2.WinForms.Guna2Button Evaluations;
        private Guna.UI2.WinForms.Guna2Button EvManage;
        private Guna.UI2.WinForms.Guna2Panel EvaluationPanel;
        private Guna.UI2.WinForms.Guna2Button guna2Button7;
        private Guna.UI2.WinForms.Guna2Button guna2Button6;
        private System.Windows.Forms.Timer EvaluateGroupTimer;
        private Guna.UI2.WinForms.Guna2Button GenerateReport;
        private Guna.UI2.WinForms.Guna2Elipse guna2Elipse1;
    }
}

