﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Mid_Project_DB.Students;
using Mid_Project_DB.Advisors;
using Mid_Project_DB.Project;
using Mid_Project_DB.Properties;
using System.Collections;
using System.Xml.Linq;
using Mid_Project_DB.Group;
using Mid_Project_DB.Evaluations;
using System.Data.SqlClient;
using System.IO;
using iTextSharp.text.pdf;
using iTextSharp.text;
using Document = iTextSharp.text.Document;
using PdfWriter = iTextSharp.text.pdf.PdfWriter;
using iText.Layout.Element;
using Paragraph = iTextSharp.text.Paragraph;
using Mid_Project_DB.BL;

namespace Mid_Project_DB
{
    public partial class Main : Form
    {
        bool maximized = true;
        private Form activeForm = null;
        bool sidebadexpand;
        bool StudentSubMenu;
        bool AdvisorsSubMenu;
        bool GroupSubMenu;
        bool evaluationMenu;
        public Main()
        {
            InitializeComponent();
        }

        private void OpenChildForm(Form childForm)
        {
            if (activeForm != null) activeForm.Close();
            activeForm = childForm;
            childForm.TopLevel = false;
            childForm.FormBorderStyle = FormBorderStyle.None;
            childForm.Dock = DockStyle.Fill;
            panelChildForm.Controls.Add(childForm);
            panelChildForm.Tag = childForm;
            childForm.BringToFront();
            childForm.Show();
        }

        private void guna2Button1_Click(object sender, EventArgs e)
        {
            if (sidebadexpand)
            { 
                StudentsTimer.Start(); 
            }
        }

        private void guna2Button2_Click(object sender, EventArgs e)
        {
            if (sidebadexpand)
            {
                AdvisorTimer.Start();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.CenterToScreen();
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            maximized = true;
        }

        private void guna2Button4_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (sidebadexpand)
            {
                flowLayoutPanel1.Width -= 5;
                if (flowLayoutPanel1.Width == flowLayoutPanel1.MinimumSize.Width)
                {
                    sidebadexpand = false;
                    SidebadAnimation.Stop();
                }
            }
            else
            {
                flowLayoutPanel1.Width += 5;
                if (flowLayoutPanel1.Width == flowLayoutPanel1.MaximumSize.Width)
                {
                    sidebadexpand = true;
                    SidebadAnimation.Stop() ;
                }
            }
        }

        private void Sidebar_Paint(object sender, PaintEventArgs e)
        {

        }

        private void MenuButton_Click(object sender, EventArgs e)
        {
            AdvisorsSubMenu = false;
            StudentSubMenu = false;
            StudentsTimer.Start();
            AdvisorTimer.Start();
            SidebadAnimation.Start();
        }

        private void panelChildForm_Paint(object sender, PaintEventArgs e)
        {

        }

        private void guna2Button3_Click(object sender, EventArgs e)
        {
            if (sidebadexpand)
            {
                OpenChildForm(new ViewProject());
            }
        }

        private void panelChildForm_Paint_1(object sender, PaintEventArgs e)
        {

        }

        private void StudentsTimer_Tick(object sender, EventArgs e)
        {
            if (StudentSubMenu)
            {
                StudentPanel.Height += 5;
                if (StudentPanel.Height >= StudentPanel.MaximumSize.Height)
                {
                    StudentSubMenu = false;
                    StudentsTimer.Stop();
                }
            }
            else
            {
                StudentPanel.Height -= 5;
                if (StudentPanel.Height == StudentPanel.MinimumSize.Height)
                {
                    StudentSubMenu = true;
                    StudentsTimer.Stop();
                }
            }
        }

        private void AddStudent_Click(object sender, EventArgs e)
        {
            OpenChildForm(new ViewStudents());
        }

        private void guna2Button1_Click_1(object sender, EventArgs e)
        {
            OpenChildForm(new ViewAdvisors());
        }

        private void AdvisorTimer_Tick(object sender, EventArgs e)
        {
            if (AdvisorsSubMenu)
            {
                AdvisorsPanel.Height += 5;
                if (AdvisorsPanel.Height >= AdvisorsPanel.MaximumSize.Height)
                {
                    AdvisorsSubMenu= false; 
                    AdvisorTimer.Stop();
                }
            }
            else
            {
                AdvisorsPanel.Height -= 5;
                if (AdvisorsPanel.Height == AdvisorsPanel.MinimumSize.Height)
                {
                    AdvisorsSubMenu = true;
                    AdvisorTimer.Stop();
                }
            }
        }





        private void Menu_Click(object sender, EventArgs e)
        {
            AdvisorsSubMenu = false;
            StudentSubMenu = false;
            GroupSubMenu= false;
            evaluationMenu = false;
            EvaluateGroupTimer.Start();
            GroupTimer.Start();
            StudentsTimer.Start();
            AdvisorTimer.Start();
            SidebadAnimation.Start();
        }

        private void guna2Button5_Click(object sender, EventArgs e)
        {
            GroupTimer.Start();
        }

        private void guna2Button8_Click(object sender, EventArgs e)
        {

        }

        private void GroupTimer_Tick(object sender, EventArgs e)
        {
            if (GroupSubMenu)
            {
                GroupsPanel.Height += 5;
                if (GroupsPanel.Height >= GroupsPanel.MaximumSize.Height)
                {
                    GroupSubMenu = false;
                    GroupTimer.Stop();
                }
            }
            else
            {
                GroupsPanel.Height -= 5;
                if (GroupsPanel.Height == GroupsPanel.MinimumSize.Height)
                {
                    GroupSubMenu = true;
                    GroupTimer.Stop();
                }
            }
        }

        private void panelChildForm_Paint_2(object sender, PaintEventArgs e)
        {

        }

        private void guna2Panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void guna2CustomGradientPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void guna2Button3_Click_1(object sender, EventArgs e)
        {
            OpenChildForm(new AddGroup());
        }



        private void flowLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void guna2ImageButton1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void guna2ImageButton2_Click(object sender, EventArgs e)
        {
            if (!maximized)
            {
                this.WindowState = FormWindowState.Maximized;
                maximized=true;
            }
            else
            {
                this.WindowState = FormWindowState.Normal;
                maximized = false;
            }
        }

        private void guna2ImageButton3_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void ViewGroup_Click(object sender, EventArgs e)
        {
            OpenChildForm(new ViewGroups());
        }

        private void Evaluations_Click(object sender, EventArgs e)
        {
            OpenChildForm(new ViewEvaluations());
        }

        private void EvManage_Click(object sender, EventArgs e)
        {
            EvaluateGroupTimer.Start();
        }

        private void EvaluateGroupTimer_Tick(object sender, EventArgs e)
        {
            if (evaluationMenu)
            {
                EvaluationPanel.Height += 5;
                if (EvaluationPanel.Height >= EvaluationPanel.MaximumSize.Height)
                {
                    evaluationMenu = false;
                    EvaluateGroupTimer.Stop();
                }
            }
            else
            {
                EvaluationPanel.Height -= 5;
                if (EvaluationPanel.Height == EvaluationPanel.MinimumSize.Height)
                {
                    evaluationMenu = true;
                    EvaluateGroupTimer.Stop();
                }
            }
        }

        private void guna2Button6_Click(object sender, EventArgs e)
        {
            OpenChildForm(new MarkEvaluation());
        }

        private void guna2Button7_Click(object sender, EventArgs e)
        {
            OpenChildForm(new ViewGroupEvaluations());
        }

        private void guna2Button4_Click_1(object sender, EventArgs e)
        {
            OpenChildForm(new AddStudentInAGroup());
        }

        //____________________________GENERATE PDF REPORT____________________//

        private void GenerateReport_Click(object sender, EventArgs e)
        {

            DataGridView AllProjects = new DataGridView();
            AllProjects.Visible = false;
            AllProjects.AllowUserToAddRows= false;
            AllProjects.ReadOnly = true;
            this.Controls.Add(AllProjects);
            AllProjects.DataSource = getAllProjects();

            //TABLE 1:-
                        PdfPTable ALLPROJECTS = getTableFromDataGridView(AllProjects); //
                         float[] width = {20f, 80f};
                         ALLPROJECTS.SetWidths(width);


            DataGridView AllStudents = new DataGridView();
            AllStudents.Visible = false;
            AllStudents.AllowUserToAddRows = false;
            AllStudents.ReadOnly = true;
            this.Controls.Add(AllStudents);
            AllStudents.DataSource = getAllStudents();

            //TABLE 2:-
                        PdfPTable ALLSTUDENTS = getTableFromDataGridView(AllStudents); // 

            DataGridView MarkSheet = new DataGridView();
            MarkSheet.Visible = false;
            MarkSheet.AllowUserToAddRows = false;
            MarkSheet.ReadOnly = true;
            this.Controls.Add(MarkSheet);
            MarkSheet.DataSource= getMarksheet();

            //TABLE 3:-
                        PdfPTable MARKSHEET = getTableFromDataGridView(MarkSheet);

            DataGridView GroupsInfo = new DataGridView();
            GroupsInfo.Visible = false;
            GroupsInfo.AllowUserToAddRows = false;
            GroupsInfo.ReadOnly = true;
            this.Controls.Add(GroupsInfo);
            GroupsInfo.DataSource = getGroupsInfo();

            //TABLE 4:- 
                         PdfPTable GROUPSINFORMATION = getTableFromDataGridView(GroupsInfo); // 

            DataGridView ProjectsBoard = new DataGridView();
            ProjectsBoard.Visible = false;
            ProjectsBoard.AllowUserToAddRows = false;
            ProjectsBoard.ReadOnly = true;
            this.Controls.Add(ProjectsBoard);
            ProjectsBoard.DataSource = getProjectAdvisors();

            //TABLE 5:-
                        PdfPTable PROJECTSBOARD = getTableFromDataGridView(ProjectsBoard); // 

            DataGridView AllAdvisors = new DataGridView();
            AllAdvisors.Visible = false;
            AllAdvisors.AllowUserToAddRows= false;
            AllAdvisors.ReadOnly = true;
            this.Controls.Add((AllAdvisors));
            AllAdvisors.DataSource = getAllAdvisors();

            //TABLE 6:-
                        PdfPTable ALLADVISORS = getTableFromDataGridView(AllAdvisors); // 

            DataGridView TotalMarksOfStudents = new DataGridView();
            TotalMarksOfStudents.Visible = false;
            TotalMarksOfStudents.AllowUserToAddRows = false;
            TotalMarksOfStudents.ReadOnly = true;
            this.Controls.Add((TotalMarksOfStudents));
            TotalMarksOfStudents.DataSource = getTotalMarks();

            //TABLE 7:-
                        PdfPTable TOTALMARKSOFSTUDENTS = getTableFromDataGridView(TotalMarksOfStudents);

            DataGridView GrouplessStudents = new DataGridView();
            GrouplessStudents.Visible = false;
            GrouplessStudents.AllowUserToAddRows = false;
            GrouplessStudents.ReadOnly = true;
            this.Controls.Add((GrouplessStudents));
            GrouplessStudents.DataSource = getGrouplessStudents();

            //TABLE 8:-
                        PdfPTable GROUPLESSsTUDENTS = getTableFromDataGridView(GrouplessStudents);

            DataGridView AllEvaluations = new DataGridView();
            AllEvaluations.Visible = false;
            AllEvaluations.AllowUserToAddRows = false;
            AllEvaluations.ReadOnly = true;
            this.Controls.Add((AllEvaluations));
            AllEvaluations.DataSource = getAllEvaluations();

            //TABLE 9:-
                        PdfPTable ALLEVALUATIONS = getTableFromDataGridView(AllEvaluations);

            DataGridView TopGroup = new DataGridView();
            TopGroup.Visible = false;
            TopGroup.AllowUserToAddRows = false;
            TopGroup.ReadOnly = true;
            this.Controls.Add((TopGroup));
            TopGroup.DataSource = getTopGroup();

            //TABLE 10:-
            PdfPTable TOPGROUP = getTableFromDataGridView(TopGroup);




            //___________________________________________________________________________DOCUMENT WRITING___________________________________________________________________________//

            iTextSharp.text.Font MainHeading = FontFactory.GetFont(iTextSharp.text.Font.FontFamily.TIMES_ROMAN.ToString(), 16, iTextSharp.text.Font.BOLD, iTextSharp.text.BaseColor.BLACK);

            SaveFileDialog save = new SaveFileDialog();

            save.Filter = "PDF (*.pdf)|*.pdf";

            save.FileName = "Report.pdf";

            bool ErrorMessage = false;

            if (save.ShowDialog() == DialogResult.OK)

            {

                if (File.Exists(save.FileName))

                {

                    try

                    {

                        File.Delete(save.FileName);

                    }

                    catch (Exception ex)

                    {

                        ErrorMessage = true;

                        MessageBox.Show("Unable to wride data in disk" + ex.Message);

                    }

                }

                if (!ErrorMessage)

                {

                    try

                    {

                        using (FileStream fileStream = new FileStream(save.FileName, FileMode.Create))

                        {

                            iTextSharp.text.Document document = new Document(PageSize.A4, 8f, 16f, 16f, 8f);

                            document.Open();

                            PdfWriter.GetInstance(document, fileStream);

                            WriteFirstPage(document);

                            Paragraph first = new Paragraph("LIST OF STUDENTS\n\n", MainHeading);
                            first.Alignment = Element.ALIGN_CENTER;
                            document.Add(first);

                            document.Add(ALLSTUDENTS);
                            document.NewPage();

                            Paragraph second = new Paragraph("LIST OF ALL PROJECTS AND THEIR DESCRIPTION\n\n", MainHeading);
                            second.Alignment = Element.ALIGN_CENTER;

                            document.Add(second);
                            document.Add(ALLPROJECTS);
                            document.NewPage();

                            Paragraph third = new Paragraph("LIST OF GROUPS WITH THEIR STUDENTS AND PROJECT\n\n", MainHeading);
                            third.Alignment = Element.ALIGN_CENTER;
                            document.Add(third);
                            document.Add(GROUPSINFORMATION);
                            document.NewPage();


                            Paragraph fourth = new Paragraph("LIST OF ALL ADVISORS\n\n", MainHeading);
                            fourth.Alignment = Element.ALIGN_CENTER;
                            document.Add(fourth);
                            document.Add(ALLADVISORS);
                            document.NewPage();

                            Paragraph fifth = new Paragraph("LIST OF ALL PROJECTS WITH THEIR RESPECTIVE ADVISORS\n\n", MainHeading);
                            fifth.Alignment = Element.ALIGN_CENTER;
                            document.Add(fifth);
                            document.Add(PROJECTSBOARD);
                            document.NewPage();

                            Paragraph tenth = new Paragraph("TOTAL EVALUATIONS\n\n", MainHeading);
                            tenth.Alignment = Element.ALIGN_CENTER;
                            document.Add(tenth);
                            document.Add(ALLEVALUATIONS);
                            document.NewPage();

                            Paragraph ninth = new Paragraph("MARKS OF STUDENTS AGAINST EACH EVALUATION\n\n", MainHeading);
                            ninth.Alignment = Element.ALIGN_CENTER;
                            document.Add(ninth);
                            document.Add(MARKSHEET);
                            document.NewPage();

                            Paragraph eleventh = new Paragraph("TOTAL MARKS OF EACH STUDENT\n\n", MainHeading);
                            eleventh.Alignment = Element.ALIGN_CENTER;
                            document.Add(eleventh);
                            document.Add(TOTALMARKSOFSTUDENTS);
                            document.NewPage();

                            Paragraph eight = new Paragraph("LIST OF ALL STUDENTS WHO NEVER JOINED A GROUP\n\n", MainHeading);
                            eight.Alignment = Element.ALIGN_CENTER;
                            document.Add(eight);
                            document.Add(GROUPLESSsTUDENTS);
                            document.NewPage();

                            Paragraph twelwth = new Paragraph("TOP GROUP\n\n", MainHeading);
                            twelwth.Alignment = Element.ALIGN_CENTER;
                            document.Add(twelwth);
                            document.Add(TOPGROUP);
                            document.NewPage();


                            document.Close();

                            fileStream.Close();

                        }

                        MessageBox.Show("Data Export Successfully", "info");

                    }

                    catch (Exception ex)

                    {

                        MessageBox.Show("Error while exporting Data" + ex.Message);

                    }

                }

            }

        }

        //_____________________TOP GROUP DATASOURCE FUNCTION_____________________//

        private DataTable getTopGroup()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd2 = new SqlCommand("select Gs.GroupId as [Group ID] ,Pr.Title as [Project Title], S.RegistrationNo  as [Registration Number], P.FirstName as [First Name], P.LastName as [Last Name], A.[Total Obtained Marks]\r\nfrom GroupStudent as Gs\r\njoin Student as S\r\non S.Id = Gs.StudentId\r\njoin Person as P\r\non P.Id = S.Id\r\njoin Lookup as L\r\non L.Id = P.Gender \r\njoin \r\n\r\n(select Top 1 Q1.[Group ID],Q2.Obtained as [Total Obtained Marks] , Q1.[Project ID]\r\nfrom (select G.Id as [Group ID] , G.Created_On ,P.Id,S.RegistrationNo, P.FirstName , P.LastName , L.Value as Gender , P.Email , Pr.Title, Pr.Id as [Project ID], Pr.Description , E.Name , Ge.ObtainedMarks ,  E.TotalMarks , E.TotalWeightage , (Ge.ObtainedMarks * E.TotalWeightage/ E.TotalMarks) as [Obtained Weightage] \r\nfrom dbo.[Group] as G\r\njoin GroupEvaluation as Ge\r\non Ge.GroupId = G.Id\r\njoin GroupProject as Gp\r\non Gp.GroupId = G.Id\r\njoin GroupStudent as Gs\r\non Gs.GroupId = G.Id\r\njoin Person as P\r\non P.Id = Gs.StudentId\r\njoin Student as S\r\non S.Id = P.Id\r\njoin Project as Pr\r\non Pr.Id = Gp.ProjectId\r\njoin Lookup as L\r\non L.Id = P.Gender\r\njoin Evaluation as E\r\non E.Id = Ge.EvaluationId\r\nwhere Gs.Status = 3\r\ngroup by G.Id , G.Created_On ,P.Id,S.RegistrationNo, P.FirstName , P.LastName ,Pr.Id, L.Value  , P.Email , Pr.Title , Pr.Description , E.Name , Ge.ObtainedMarks ,  E.TotalMarks , E.TotalWeightage , (Ge.ObtainedMarks * E.TotalWeightage/ E.TotalMarks )\r\n) as Q1\r\n\r\njoin (select SUM(S.[Obtained Weightage]) as Obtained ,  S.Id\r\nfrom dbo.[Group] as G\r\njoin (select G.Id as [Group ID] , G.Created_On ,P.Id , P.FirstName , P.LastName , L.Value as Gender , P.Email , Pr.Title , Pr.Description , E.Name , Ge.ObtainedMarks ,  E.TotalMarks , E.TotalWeightage , (Ge.ObtainedMarks * E.TotalWeightage/ E.TotalMarks) as [Obtained Weightage] \r\nfrom dbo.[Group] as G\r\njoin GroupEvaluation as Ge\r\non Ge.GroupId = G.Id\r\njoin GroupProject as Gp\r\non Gp.GroupId = G.Id\r\njoin GroupStudent as Gs\r\non Gs.GroupId = G.Id\r\njoin Person as P\r\non P.Id = Gs.StudentId\r\njoin Project as Pr\r\non Pr.Id = Gp.ProjectId\r\njoin Lookup as L\r\non L.Id = P.Gender\r\njoin Evaluation as E\r\non E.Id = Ge.EvaluationId\r\nwhere Gs.Status = 3\r\ngroup by G.Id , G.Created_On ,P.Id , P.FirstName , P.LastName , L.Value  , P.Email , Pr.Title , Pr.Description , E.Name , Ge.ObtainedMarks ,  E.TotalMarks , E.TotalWeightage , (Ge.ObtainedMarks * E.TotalWeightage/ E.TotalMarks )\r\n) as S\r\non S.[Group ID] = G.Id\r\ngroup by S.FirstName , S.LastName  , S.Id) as Q2\r\n\r\non Q1.Id = Q2.Id\r\nGroup by Q1.[Group ID], Q2.Obtained , Q1.Title , Q1.[Project ID]\r\nHaving  Q2.Obtained = Max(Q2.Obtained)\r\nOrder by [Total Obtained Marks] desc\r\n) as A\r\n\r\njoin Project as Pr\r\non Pr.Id = A.[Project ID]\r\non A.[Group ID] = Gs.GroupId\r\nwhere Gs.Status = 3\r\nGroup by  Gs.GroupId ,Pr.Title, S.RegistrationNo , P.FirstName , P.LastName , P.Contact , P.Email , L.Value , A.[Total Obtained Marks]", con);
            SqlDataReader sqlData2 = cmd2.ExecuteReader();
            DataTable dataTable2 = new DataTable();
            dataTable2.Load(sqlData2);
            return dataTable2;
        }


        //_____________________ALL EVALUATIONS DATASOURCE FUNCTION_____________________//

        private DataTable getAllEvaluations()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd2 = new SqlCommand("select Name , TotalMarks as [Total Marks] , TotalWeightage as [Total Weightage]\r\nfrom Evaluation", con);
            SqlDataReader sqlData2 = cmd2.ExecuteReader();
            DataTable dataTable2 = new DataTable();
            dataTable2.Load(sqlData2);
            return dataTable2;
        }

        //_____________________STUDENTS WHO NEVER JOINED A GROUP DATASOURCE FUNCTION_____________________//

        private DataTable getGrouplessStudents()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd2 = new SqlCommand("Select S.RegistrationNo as [Registration Number] , P.FirstName as [First Name], P.LastName as [Last Name], P.Contact , P.Email , L.Value as Gender\r\nfrom Person as P\r\njoin Student as S\r\non S.Id = P.Id\r\njoin Lookup as L\r\non L.Id = P.Gender\r\n\r\nEXCEPT\r\n\r\nSelect S.RegistrationNo , P.FirstName , P.LastName , P.Contact , P.Email , L.Value as Gender\r\nfrom GroupStudent as Gs\r\njoin Person as P\r\non P.Id = Gs.StudentId\r\njoin Student as S\r\non S.Id = P.Id\r\njoin Lookup as L\r\non L.Id = P.Gender", con);
            SqlDataReader sqlData2 = cmd2.ExecuteReader();
            DataTable dataTable2 = new DataTable();
            dataTable2.Load(sqlData2);
            return dataTable2;
        }

        //_____________________MARKSHEET DATASOURCE FUNCTION_____________________//

        private DataTable getMarksheet()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd2 = new SqlCommand("select Pr.Title as [Project Title] , S.RegistrationNo as [Registration Number] , P.FirstName  as [First Name], P.LastName  as [Last Name], E.Name as [Evaluation] , Ge.ObtainedMarks as [Obtained Marks] , E.TotalMarks as [Total Marks] , Ge.EvaluationDate as [Evaluation Date]\r\nfrom [dbo].[Group] as G\r\njoin GroupStudent as Gs\r\non Gs.GroupId = G.Id\r\njoin GroupEvaluation as Ge\r\non Ge.GroupId = G.Id\r\njoin GroupProject as Gp\r\non Gp.GroupId = G.Id\r\njoin Person as P\r\non P.Id = Gs.StudentId\r\njoin Student as S\r\non S.Id = P.Id\r\njoin Evaluation as E\r\non E.Id = Ge.EvaluationId\r\njoin Project as Pr\r\non Pr.Id = Gp.ProjectId\r\nwhere Gs.Status = 3\r\ngroup by  Pr.Title , S.RegistrationNo , P.FirstName , P.LastName , E.Name , Ge.ObtainedMarks , E.TotalMarks , Ge.EvaluationDate", con);
            SqlDataReader sqlData2 = cmd2.ExecuteReader();
            DataTable dataTable2 = new DataTable();
            dataTable2.Load(sqlData2);
            return dataTable2;
        }


        //_____________________TOTAL MARKS OF EACH STUDENT DATASOURCE FUNCTION_____________________//

        private DataTable getTotalMarks()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd2 = new SqlCommand("select Q1.RegistrationNo as [Registration Number], Q1.FirstName , Q1.LastName , Q1.Gender , Q1.Email  ,Q2.Obtained as [Total Obtained Marks] \r\nfrom (select G.Id as [Group ID] , G.Created_On ,P.Id,S.RegistrationNo, P.FirstName , P.LastName , L.Value as Gender , P.Email , Pr.Title , Pr.Description , E.Name , Ge.ObtainedMarks ,  E.TotalMarks , E.TotalWeightage , (Ge.ObtainedMarks * E.TotalWeightage/ E.TotalMarks) as [Obtained Weightage] \r\nfrom dbo.[Group] as G\r\njoin GroupEvaluation as Ge\r\non Ge.GroupId = G.Id\r\njoin GroupProject as Gp\r\non Gp.GroupId = G.Id\r\njoin GroupStudent as Gs\r\non Gs.GroupId = G.Id\r\njoin Person as P\r\non P.Id = Gs.StudentId\r\njoin Student as S\r\non S.Id = P.Id\r\njoin Project as Pr\r\non Pr.Id = Gp.ProjectId\r\njoin Lookup as L\r\non L.Id = P.Gender\r\njoin Evaluation as E\r\non E.Id = Ge.EvaluationId\r\nwhere Gs.Status = 3\r\ngroup by G.Id , G.Created_On ,P.Id,S.RegistrationNo, P.FirstName , P.LastName , L.Value  , P.Email , Pr.Title , Pr.Description , E.Name , Ge.ObtainedMarks ,  E.TotalMarks , E.TotalWeightage , (Ge.ObtainedMarks * E.TotalWeightage/ E.TotalMarks )\r\n) as Q1\r\n\r\njoin (select SUM(S.[Obtained Weightage]) as Obtained ,  S.Id\r\nfrom dbo.[Group] as G\r\njoin (select G.Id as [Group ID] , G.Created_On ,P.Id , P.FirstName , P.LastName , L.Value as Gender , P.Email , Pr.Title , Pr.Description , E.Name , Ge.ObtainedMarks ,  E.TotalMarks , E.TotalWeightage , (Ge.ObtainedMarks * E.TotalWeightage/ E.TotalMarks) as [Obtained Weightage] \r\nfrom dbo.[Group] as G\r\njoin GroupEvaluation as Ge\r\non Ge.GroupId = G.Id\r\njoin GroupProject as Gp\r\non Gp.GroupId = G.Id\r\njoin GroupStudent as Gs\r\non Gs.GroupId = G.Id\r\njoin Person as P\r\non P.Id = Gs.StudentId\r\njoin Project as Pr\r\non Pr.Id = Gp.ProjectId\r\njoin Lookup as L\r\non L.Id = P.Gender\r\njoin Evaluation as E\r\non E.Id = Ge.EvaluationId\r\nwhere Gs.Status = 3\r\ngroup by G.Id , G.Created_On ,P.Id , P.FirstName , P.LastName , L.Value  , P.Email , Pr.Title , Pr.Description , E.Name , Ge.ObtainedMarks ,  E.TotalMarks , E.TotalWeightage , (Ge.ObtainedMarks * E.TotalWeightage/ E.TotalMarks )\r\n) as S\r\non S.[Group ID] = G.Id\r\ngroup by S.FirstName , S.LastName  , S.Id) as Q2\r\n\r\non Q1.Id = Q2.Id\r\nGroup by Q1.RegistrationNo, Q1.FirstName , Q1.LastName , Q1.Gender , Q1.Email  , Q2.Obtained", con);
            SqlDataReader sqlData2 = cmd2.ExecuteReader();
            DataTable dataTable2 = new DataTable();
            dataTable2.Load(sqlData2);
            return dataTable2;
        }

        //_____________________PROJECTS WITH THEIR ADVISORS DATASOURCE FUNCTION_____________________//
        private DataTable getProjectAdvisors()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd2 = new SqlCommand("select P.Title as [Project Title]   , P1.FirstName as [First Name] , P1.LastName as [Last Name] , P1.Email , P1.Contact, L.Value as Gender, L1.Value as [Role in Project]\r\nfrom Project as P\r\njoin ProjectAdvisor as Pa\r\non P.Id = Pa.ProjectId\r\njoin Person as P1\r\non P1.Id = Pa.AdvisorId\r\njoin Lookup as L\r\non L.Id = P1.Gender\r\njoin Lookup as L1\r\non L1.Id = Pa.AdvisorRole\r\njoin Advisor as A\r\non A.Id = P1.Id\r\njoin Lookup as L2\r\non L2.Id = A.Designation\r\ngroup by P.Title , P.Description , L1.Value , P1.FirstName , P1.LastName , P1.Email , L.Value , Pa.AssignmentDate , P1.Contact\r\n", con);
            SqlDataReader sqlData2 = cmd2.ExecuteReader();
            DataTable dataTable2 = new DataTable();
            dataTable2.Load(sqlData2);
            return dataTable2;
        }

        //_____________________ALL ADVISORS DATASOURCE FUNCTION_____________________//
        private DataTable getAllAdvisors()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd2 = new SqlCommand("select P.FirstName as [First Name], P.LastName as [Last Name], L1.Value as [Gender], P.Email , P.Contact , L.Value as Designation\r\nfrom Advisor as A\r\njoin Person as P\r\non P.Id = A.Id \r\njoin Lookup as L\r\non A.Designation = L.Id\r\njoin Lookup as L1\r\non L1.Id = P.Gender", con);
            SqlDataReader sqlData2 = cmd2.ExecuteReader();
            DataTable dataTable2 = new DataTable();
            dataTable2.Load(sqlData2);
            return dataTable2;
        }

        //_____________________ALL PROJECTS WITH THEIR ADVISORS DATASOURCE FUNCTION__________________//
        private DataTable getAllProjectsWithStudents()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select P.Title as [Project Title]   , P1.FirstName as [First Name], P1.LastName as [Last Name], P1.Email , L.Value as Gender, L1.Value as Role\r\nfrom Project as P\r\njoin ProjectAdvisor as Pa\r\non P.Id = Pa.ProjectId\r\njoin Person as P1\r\non P1.Id = Pa.AdvisorId\r\njoin Lookup as L\r\non L.Id = P1.Gender\r\njoin Lookup as L1\r\non L1.Id = Pa.AdvisorRole\r\ngroup by P.Title , P.Description , L1.Value , P1.FirstName , P1.LastName , P1.Email , L.Value , Pa.AssignmentDate", con);
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            return dataTable;
        }

        //_____________________ALL STUDENTS DATASOURCE FUNCTION__________________//

        private DataTable getAllStudents()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd1 = new SqlCommand("select S.RegistrationNo as [Registration Number] , P.FirstName as [First Name], P.LastName as [Last Name], P.Contact , P.Email  , L.Value as Gender\r\nfrom Student as S\r\njoin Person as P\r\non S.Id = P.Id\r\njoin Lookup as L\r\non L.Id = P.Gender", con);
            SqlDataReader sqlData1 = cmd1.ExecuteReader();
            DataTable dataTable1 = new DataTable();
            dataTable1.Load(sqlData1);
            return dataTable1;
        }


        //_____________________ALL PROJECTS DATASOURCE FUNCTION__________________//
        private DataTable getAllProjects()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select P.Title , P.Description from Project as P", con);
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            return dataTable;
        }

        //_____________________ALL GROUPS INFORMATION DATASOURCE FUNCTION__________________//
        private DataTable getGroupsInfo()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select G.Id as [Group ID] , G.Created_On as [Created On] , Pr.Title  as [Project Title] , S.RegistrationNo as [Registration Number] , P.FirstName as [First Name] , P.LastName as [Last Name]\r\nfrom [dbo].[Group] as G\r\njoin GroupProject as Gp\r\non Gp.GroupId = G.Id\r\njoin GroupStudent as Gs\r\non Gs.GroupId = G.Id\r\njoin Person as P\r\non P.Id = Gs.StudentId\r\njoin Project as Pr\r\non Pr.Id = Gp.ProjectId\r\njoin Student as S\r\non S.Id = P.Id\r\nwhere Gs.Status = 3\r\nGroup by  G.Id , G.Created_On , Pr.Title , S.RegistrationNo , P.FirstName , P.LastName", con);
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            return dataTable;
        }


        //__________________ACCEPTS A DATAGRIDVIEW AND RETURNS A PDFTABLE____________//
        private PdfPTable getTableFromDataGridView(DataGridView datagridview) 
        {
            PdfPTable Table = new PdfPTable(datagridview.Columns.Count);

            if (datagridview.DataSource != null)
            {
                foreach (DataGridViewColumn col in datagridview.Columns)

                {

                    PdfPCell pCell = new PdfPCell(new Phrase(col.HeaderText));
                    pCell.Phrase.Font.SetStyle("bold");
                    Table.AddCell(pCell);

                }

                if (datagridview.Rows.Count > 0)
                {

                    Table.DefaultCell.Padding = 2;

                    Table.WidthPercentage = 100;

                    Table.HorizontalAlignment = Element.ALIGN_LEFT;



                    foreach (DataGridViewRow viewRow in datagridview.Rows)

                    {

                        foreach (DataGridViewCell dcell in viewRow.Cells)

                        {

                            Table.AddCell(dcell.Value.ToString());

                        }

                    }

                    for (int j = 0; j < Table.Rows.Count; j++)
                    {
                        for (int i = 0; i < Table.NumberOfColumns; i++)
                        {
                            if (j == 0)
                            {
                                Table.Rows[j].GetCells()[i].BackgroundColor = BaseColor.LIGHT_GRAY;
                            }

                            Table.Rows[j].GetCells()[i].Column.Alignment = Element.ALIGN_CENTER;
                        }
                    }
                }

            }
                return Table;
            
        }

        private void WriteFirstPage(Document document)
        {
            document.Open();

            document.Add(new Paragraph("\n\n\n"));

            document.AddTitle("FInal Year Project Management System by Zainab Idrees (2022-CS-199)");

            iTextSharp.text.Font font = FontFactory.GetFont(iTextSharp.text.Font.FontFamily.TIMES_ROMAN.ToString(), 20, iTextSharp.text.Font.BOLD, iTextSharp.text.BaseColor.BLACK);
            iTextSharp.text.Font font1 = FontFactory.GetFont(iTextSharp.text.Font.FontFamily.TIMES_ROMAN.ToString(), 18, iTextSharp.text.Font.BOLD, iTextSharp.text.BaseColor.BLACK);
            iTextSharp.text.Font font112 = FontFactory.GetFont(iTextSharp.text.Font.FontFamily.TIMES_ROMAN.ToString(), 16, iTextSharp.text.Font.NORMAL, iTextSharp.text.BaseColor.BLACK);

            iTextSharp.text.Paragraph elements = new iTextSharp.text.Paragraph("FYP MANAGEMENT SYSTEM\n", font);
            elements.Alignment = Element.ALIGN_CENTER;
            document.Add(elements);


            document.Add(new Paragraph("\n\n\n"));

            System.Drawing.Image image = Properties.Resources.Logo;
            iTextSharp.text.Image image1 = iTextSharp.text.Image.GetInstance(image, System.Drawing.Imaging.ImageFormat.Png);
            image1.Alignment = Element.ALIGN_CENTER;
            image1.ScaleAbsolute(150f, 150f);
            document.Add(image1);


            Paragraph elements1 = new Paragraph("session 2022-2026");
            elements1.Alignment = Element.ALIGN_CENTER;
            document.Add(elements1);

            document.Add(new Paragraph("\n\n"));

            Paragraph elements11 = new Paragraph("Report and Project made by:\n\n", font1);
            elements11.Alignment = Element.ALIGN_CENTER;
            document.Add(elements11);



            Paragraph elements111 = new Paragraph("Zainab Idrees \t 2022-CS-199\n", font112);
            elements111.Alignment = Element.ALIGN_CENTER;
            document.Add(elements111);

            document.Add(new Paragraph("\n\n\n"));

            Paragraph elements112 = new Paragraph("Supervised By:\n\n", font1);
            elements112.Alignment = Element.ALIGN_CENTER;
            document.Add(elements112);

            Paragraph elements1112 = new Paragraph("Sir Nazeef Ul Haq\n", font112);
            elements1112.Alignment = Element.ALIGN_CENTER;
            document.Add(elements1112);

            document.Add(new Paragraph("\n\n"));
            document.Add(new Paragraph("\n\n"));

            Paragraph css = new Paragraph("Department of Computer Science\n", font112);
            css.Alignment = Element.ALIGN_CENTER;
            document.Add(css);

            iTextSharp.text.Paragraph element = new iTextSharp.text.Paragraph("University of Engineering and Technology\r\nLahore Pakistan\n", font);
            element.Alignment = Element.ALIGN_CENTER;
            document.Add(element);


            document.NewPage();
        }

        private void guna2CustomGradientPanel1_DragDrop(object sender, DragEventArgs e)
        {
            maximized = false;
        }
    }
}
