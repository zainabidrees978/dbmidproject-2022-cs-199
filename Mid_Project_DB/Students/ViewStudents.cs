﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mid_Project_DB.Students
{
    public partial class ViewStudents : Form
    {
        private static int ID = -1;
        string FirstName , RegistrationNumber;
        string LastName, Contact, Email, DOB;
        int GEnder;
        public ViewStudents()
        {
            InitializeComponent();
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void ViewStudents_Load(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select S.Id , S.RegistrationNo , P.FirstName , P.LastName , P.Contact , P.DateOfBirth , P.Email , L.Value as Gender \r\nfrom Student as S\r\njoin Person as P\r\non S.Id = P.Id\r\njoin Lookup as L\r\non L.Id = P.Gender");
            cmd.Connection = con;
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable= new DataTable();
            dataTable.Load(sqlData);
            AllStudents.DataSource = dataTable;
            AllStudents.RowHeadersVisible= true;
            AllStudents.RowHeadersWidth = 30;   
        }

        private void guna2Button1_Click(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void guna2Button2_Click(object sender, EventArgs e)
        {

        }

        private void guna2Button1_Click_1(object sender, EventArgs e)
        {
            Form form = new AddStudent();
            form.ShowDialog();
        }

        private void guna2Button3_Click(object sender, EventArgs e)
        {
            if (ID != -1)
            {
                try
                {
                    if (MessageBox.Show("Are you sure?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        var con = Configuration.getInstance().getConnection();
                        SqlCommand cmd = new SqlCommand("delete [dbo].[GroupStudent] where StudentId = @ID ; delete [dbo].[Student] where Id = @ID; delete [dbo].[Person] where Id = @ID; ", con);
                        cmd.Parameters.AddWithValue("@ID", ID);
                        cmd.Connection = con;
                        cmd.ExecuteNonQuery();
                        MessageBox.Show("Record Has Been Deleted Successfully...");
                        ID = -1;
                        refresh();
                    }
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }

            }
            else
            {
                MessageBox.Show("Please Select A Row To Delete...");
            }
        }


        private void guna2Button5_Click(object sender, EventArgs e)
        {
            try
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("select S.Id , S.RegistrationNo , P.FirstName , P.LastName , P.Contact , P.DateOfBirth , P.Email , L.Value as Gender \r\nfrom Student as S\r\njoin Person as P\r\non S.Id = P.Id\r\njoin Lookup as L\r\non L.Id = P.Gender where S.RegistrationNo = '" + txtSearch.Text + "'");
                cmd.Connection = con;
                SqlDataReader sqlData = cmd.ExecuteReader();
                DataTable dataTable = new DataTable();
                dataTable.Load(sqlData);
                AllStudents.DataSource = dataTable;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }



        private void txtSearch_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13)
            {
                try
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("select S.Id , S.RegistrationNo , P.FirstName , P.LastName , P.Contact , P.DateOfBirth , P.Email , L.Value as Gender \r\nfrom Student as S\r\njoin Person as P\r\non S.Id = P.Id\r\njoin Lookup as L\r\non L.Id = P.Gender where S.RegistrationNo = '" + txtSearch.Text + "'");
                    cmd.Connection = con;
                    SqlDataReader sqlData = cmd.ExecuteReader();
                    DataTable dataTable = new DataTable();
                    dataTable.Load(sqlData);
                    AllStudents.DataSource = dataTable;
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void AllStudents_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowIndex = e.RowIndex;
            DataGridViewRow row = AllStudents.Rows[rowIndex];
            ID = int.Parse(AllStudents.Rows[e.RowIndex].Cells[0].Value.ToString());
            RegistrationNumber = AllStudents.Rows[e.RowIndex].Cells[1].Value.ToString();
            FirstName = AllStudents.Rows[e.RowIndex].Cells[2].Value.ToString();
            LastName = AllStudents.Rows[e.RowIndex].Cells[3].Value.ToString();
            Contact = AllStudents.Rows[e.RowIndex].Cells[4].Value.ToString();
            DOB = AllStudents.Rows[e.RowIndex].Cells[5].Value.ToString();
            Email = AllStudents.Rows[e.RowIndex].Cells[6].Value.ToString();
            if (AllStudents.Rows[e.RowIndex].Cells[7].Value.ToString() == "Male")
            {
                GEnder = 1;
            }
            else { GEnder = 2; }
        }

        private void AllStudents_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int rowIndex = e.RowIndex;
            DataGridViewRow row = AllStudents.Rows[rowIndex];
            ID = int.Parse(AllStudents.Rows[e.RowIndex].Cells[0].Value.ToString());
            RegistrationNumber = AllStudents.Rows[e.RowIndex].Cells[1].Value.ToString();
            FirstName = AllStudents.Rows[e.RowIndex].Cells[2].Value.ToString();
            LastName = AllStudents.Rows[e.RowIndex].Cells[3].Value.ToString();
            Contact = AllStudents.Rows[e.RowIndex].Cells[4].Value.ToString();
            DOB = AllStudents.Rows[e.RowIndex].Cells[5].Value.ToString();
            Email = AllStudents.Rows[e.RowIndex].Cells[6].Value.ToString();
            if (AllStudents.Rows[e.RowIndex].Cells[7].Value.ToString() == "Male")
            {
                GEnder = 1;
            }
            else { GEnder = 2; }
        }

        private void AllStudents_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int rowIndex = e.RowIndex;
            DataGridViewRow row = AllStudents.Rows[rowIndex];
            ID = int.Parse(AllStudents.Rows[e.RowIndex].Cells[0].Value.ToString());
            RegistrationNumber = AllStudents.Rows[e.RowIndex].Cells[1].Value.ToString();
            FirstName = AllStudents.Rows[e.RowIndex].Cells[2].Value.ToString();
            LastName = AllStudents.Rows[e.RowIndex].Cells[3].Value.ToString();
            Contact = AllStudents.Rows[e.RowIndex].Cells[4].Value.ToString();
            DOB = AllStudents.Rows[e.RowIndex].Cells[5].Value.ToString();
            Email = AllStudents.Rows[e.RowIndex].Cells[6].Value.ToString();
            if (AllStudents.Rows[e.RowIndex].Cells[7].Value.ToString() == "Male")
            {
                GEnder = 1;
            }
            else { GEnder = 2; }
        }

        private void AllStudents_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView1_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
           
        }

        private void guna2Button4_Click(object sender, EventArgs e)
        {
            refresh();
        }

        private void refresh()
        {
            AllStudents.DataSource= null;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select S.Id , S.RegistrationNo , P.FirstName , P.LastName , P.Contact , P.DateOfBirth , P.Email , L.Value as Gender \r\nfrom Student as S\r\njoin Person as P\r\non S.Id = P.Id\r\njoin Lookup as L\r\non L.Id = P.Gender");
            cmd.Connection = con;
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            AllStudents.DataSource = dataTable;
        }

        private void guna2Button2_Click_1(object sender, EventArgs e)
        {
            if (ID != -1)
            {
                Form form = new EditStudent(FirstName, LastName, Contact, Email, DOB, GEnder, RegistrationNumber, ID);
                form.ShowDialog();
                ID = -1;
            }
            else
            {
                MessageBox.Show("Please select an entry to edit...");
            }
        }
    }
}
