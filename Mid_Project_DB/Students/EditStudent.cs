﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mid_Project_DB.Students
{
    public partial class EditStudent : Form
    {
        private static int IDs;
        int genderI = 0;
        public EditStudent(string FirstName , string LastName , string Contact , string email, string DOB , int gender , string RegistrationNumber , int ID)
        {
            InitializeComponent();
            txtContactNumber.Text = Contact;
            txtEmail.Text = email;
            txtFirstName.Text = FirstName;
            txtLastName.Text = LastName;
            txtRegNo.Text = RegistrationNumber;
            genderI = gender -1;
            guna2DateTimePicker1.Value = DateTime.Parse(DOB);
            IDs = ID;
        }

        private void EditStudent_Load(object sender, EventArgs e)
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            this.CenterToScreen();

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select Value from Lookup where Category = 'GENDER'");
            cmd.Connection = con;
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            List<string> list = new List<string>();
            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                string s = dataTable.Rows[i]["Value"].ToString();
                list.Add(s);
            }
            Gender.DataSource = list;
            Gender.SelectedIndex = genderI;
        }

        private void buttonAddStudent_Click(object sender, EventArgs e)
        {
            bool flag = true;

            if (flag)
            {
                if (string.IsNullOrEmpty(txtContactNumber.Text) || string.IsNullOrEmpty(txtEmail.Text) || string.IsNullOrEmpty(txtFirstName.Text) || string.IsNullOrEmpty(txtLastName.Text) || string.IsNullOrEmpty(txtRegNo.Text))
                {
                    MessageBox.Show("Please Fill All The Queries...");
                    flag = false;
                }
            }
            try
            {
                if (flag)
                {
                    if (!string.IsNullOrEmpty(txtContactNumber.Text) && !string.IsNullOrEmpty(txtEmail.Text) && !string.IsNullOrEmpty(txtFirstName.Text) && !string.IsNullOrEmpty(txtLastName.Text) && !string.IsNullOrEmpty(txtRegNo.Text))
                    {
                        int gender = 0;
                        var con = Configuration.getInstance().getConnection();
                        SqlCommand cmd = new SqlCommand("update [dbo].[Person] " +
                        " set FirstName = @FirstName, LastName = @LastName, Contact = @Contact, Email = @Email, DateOfBirth = @DOB, Gender = @Gender " +
                        " where Id = @ID " +
                        " update [dbo].[Student]" +
                        " set RegistrationNo = @RegistrationNumber" +
                        " where Id = @ID ", con);
                        cmd.Parameters.AddWithValue("@FirstName", txtFirstName.Text);
                        cmd.Parameters.AddWithValue("@LastName", txtLastName.Text);
                        cmd.Parameters.AddWithValue("@Contact", txtContactNumber.Text);
                        cmd.Parameters.AddWithValue("@Email", txtEmail.Text);
                        if (Gender.SelectedIndex == 0)
                        {
                            gender = 1;
                        }
                        else if (Gender.SelectedIndex == 1)
                        {
                            gender = 2;
                        }
                        cmd.Parameters.AddWithValue("@Gender", gender);
                        cmd.Parameters.AddWithValue("@ID", IDs);
                        guna2DateTimePicker1.Format = DateTimePickerFormat.Custom;
                        guna2DateTimePicker1.CustomFormat = "yyyy-MM-dd";
                        cmd.Parameters.AddWithValue("@DOB", guna2DateTimePicker1.Text);
                        cmd.Parameters.AddWithValue("@RegistrationNumber", txtRegNo.Text);
                        cmd.ExecuteNonQuery();
                        MessageBox.Show("Successfully Updated");
                        this.Close();
                    }
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }


        }

        private void guna2Button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
